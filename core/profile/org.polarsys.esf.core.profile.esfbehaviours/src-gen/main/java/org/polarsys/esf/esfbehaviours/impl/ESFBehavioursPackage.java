/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfbehaviours.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.uml.UMLPackage;

import org.polarsys.esf.esfbehaviours.IAbstractSBehaviourObject;
import org.polarsys.esf.esfbehaviours.IESFBehavioursFactory;
import org.polarsys.esf.esfbehaviours.IESFBehavioursPackage;
import org.polarsys.esf.esfbehaviours.ISEvent;
import org.polarsys.esf.esfbehaviours.ISState;

import org.polarsys.esf.esfcore.IESFCorePackage;

import org.polarsys.esf.esfproperties.IESFPropertiesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ESFBehavioursPackage
    extends EPackageImpl
    implements IESFBehavioursPackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSBehaviourObjectEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sEventEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sStateEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.polarsys.esf.esfbehaviours.IESFBehavioursPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ESFBehavioursPackage() {
        super(eNS_URI, IESFBehavioursFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>
     * This method is used to initialize {@link IESFBehavioursPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static IESFBehavioursPackage init() {
        if (isInited)
            return (IESFBehavioursPackage) EPackage.Registry.INSTANCE.getEPackage(IESFBehavioursPackage.eNS_URI);

        // Obtain or create and register package
        ESFBehavioursPackage theESFBehavioursPackage =
            (ESFBehavioursPackage) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ESFBehavioursPackage
                ? EPackage.Registry.INSTANCE.get(eNS_URI)
                : new ESFBehavioursPackage());

        isInited = true;

        // Initialize simple dependencies
        IESFCorePackage.eINSTANCE.eClass();
        IESFPropertiesPackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theESFBehavioursPackage.createPackageContents();

        // Initialize created meta-data
        theESFBehavioursPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theESFBehavioursPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(IESFBehavioursPackage.eNS_URI, theESFBehavioursPackage);
        return theESFBehavioursPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getAbstractSBehaviourObject() {
        return abstractSBehaviourObjectEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSEvent() {
        return sEventEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEvent_Base_Classifier() {
        return (EReference) sEventEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSEvent_Probability() {
        return (EReference) sEventEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSState() {
        return sStateEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSState_Base_Classifier() {
        return (EReference) sStateEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public IESFBehavioursFactory getESFBehavioursFactory() {
        return (IESFBehavioursFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        abstractSBehaviourObjectEClass = createEClass(ABSTRACT_SBEHAVIOUR_OBJECT);

        sEventEClass = createEClass(SEVENT);
        createEReference(sEventEClass, SEVENT__BASE_CLASSIFIER);
        createEReference(sEventEClass, SEVENT__PROBABILITY);

        sStateEClass = createEClass(SSTATE);
        createEReference(sStateEClass, SSTATE__BASE_CLASSIFIER);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        IESFCorePackage theESFCorePackage =
            (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);
        UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
        IESFPropertiesPackage theESFPropertiesPackage =
            (IESFPropertiesPackage) EPackage.Registry.INSTANCE.getEPackage(IESFPropertiesPackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        abstractSBehaviourObjectEClass.getESuperTypes().add(theESFCorePackage.getAbstractSElement());
        sEventEClass.getESuperTypes().add(this.getAbstractSBehaviourObject());
        sStateEClass.getESuperTypes().add(this.getAbstractSBehaviourObject());

        // Initialize classes, features, and operations; add parameters
        initEClass(
            abstractSBehaviourObjectEClass,
            IAbstractSBehaviourObject.class,
            "AbstractSBehaviourObject", //$NON-NLS-1$
            IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);

        initEClass(sEventEClass, ISEvent.class, "SEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSEvent_Base_Classifier(),
            theUMLPackage.getClassifier(),
            null,
            "base_Classifier", //$NON-NLS-1$
            null,
            1,
            1,
            ISEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSEvent_Probability(),
            theESFPropertiesPackage.getSProbability(),
            null,
            "probability", //$NON-NLS-1$
            null,
            1,
            1,
            ISEvent.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sStateEClass, ISState.class, "SState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEReference(
            getSState_Base_Classifier(),
            theUMLPackage.getClassifier(),
            null,
            "base_Classifier", //$NON-NLS-1$
            null,
            1,
            1,
            ISState.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http://www.eclipse.org/uml2/2.0.0/UML
        createUMLAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createUMLAnnotations() {
        String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
        addAnnotation(this, source, new String[] {"originalName", "ESFBehaviours" //$NON-NLS-1$ //$NON-NLS-2$
        });
    }

} // ESFBehavioursPackage
