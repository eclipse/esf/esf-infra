/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfbehaviours;

import org.eclipse.uml2.uml.Classifier;

import org.polarsys.esf.esfproperties.ISProbability;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SEvent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfbehaviours.ISEvent#getBase_Classifier <em>Base Classifier</em>}</li>
 * <li>{@link org.polarsys.esf.esfbehaviours.ISEvent#getProbability <em>Probability</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfbehaviours.IESFBehavioursPackage#getSEvent()
 * @model
 * @generated
 */
public interface ISEvent
    extends IAbstractSBehaviourObject {

    /**
     * Returns the value of the '<em><b>Base Classifier</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Classifier</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Classifier</em>' reference.
     * @see #setBase_Classifier(Classifier)
     * @see org.polarsys.esf.esfbehaviours.IESFBehavioursPackage#getSEvent_Base_Classifier()
     * @model required="true" ordered="false"
     * @generated
     */
    Classifier getBase_Classifier();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfbehaviours.ISEvent#getBase_Classifier <em>Base Classifier</em>}
     * ' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Classifier</em>' reference.
     * @see #getBase_Classifier()
     * @generated
     */
    void setBase_Classifier(Classifier value);

    /**
     * Returns the value of the '<em><b>Probability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Probability</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Probability</em>' reference.
     * @see #setProbability(ISProbability)
     * @see org.polarsys.esf.esfbehaviours.IESFBehavioursPackage#getSEvent_Probability()
     * @model required="true" ordered="false"
     * @generated
     */
    ISProbability getProbability();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfbehaviours.ISEvent#getProbability <em>Probability</em>}'
     * reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Probability</em>' reference.
     * @see #getProbability()
     * @generated
     */
    void setProbability(ISProbability value);

} // ISEvent
