/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfbehaviours.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.esf.esfbehaviours.IAbstractSBehaviourObject;
import org.polarsys.esf.esfbehaviours.IESFBehavioursPackage;

import org.polarsys.esf.esfcore.impl.AbstractSElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SBehaviour Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AbstractSBehaviourObject
    extends AbstractSElement
    implements IAbstractSBehaviourObject {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractSBehaviourObject() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IESFBehavioursPackage.Literals.ABSTRACT_SBEHAVIOUR_OBJECT;
    }

} // AbstractSBehaviourObject
