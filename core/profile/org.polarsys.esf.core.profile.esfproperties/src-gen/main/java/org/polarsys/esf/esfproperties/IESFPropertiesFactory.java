/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfproperties.IESFPropertiesPackage
 * @generated
 */
public interface IESFPropertiesFactory
    extends EFactory {

    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    IESFPropertiesFactory eINSTANCE = org.polarsys.esf.esfproperties.impl.ESFPropertiesFactory.init();

    /**
     * Returns a new object of class '<em>SCriticality</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SCriticality</em>'.
     * @generated
     */
    ISCriticality createSCriticality();

    /**
     * Returns a new object of class '<em>SSeverity</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SSeverity</em>'.
     * @generated
     */
    ISSeverity createSSeverity();

    /**
     * Returns a new object of class '<em>SOccurence</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SOccurence</em>'.
     * @generated
     */
    ISOccurence createSOccurence();

    /**
     * Returns a new object of class '<em>SDetectability</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SDetectability</em>'.
     * @generated
     */
    ISDetectability createSDetectability();

    /**
     * Returns a new object of class '<em>SProbability</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SProbability</em>'.
     * @generated
     */
    ISProbability createSProbability();

    /**
     * Returns a new object of class '<em>SCost</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>SCost</em>'.
     * @generated
     */
    ISCost createSCost();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    IESFPropertiesPackage getESFPropertiesPackage();

} // IESFPropertiesFactory
