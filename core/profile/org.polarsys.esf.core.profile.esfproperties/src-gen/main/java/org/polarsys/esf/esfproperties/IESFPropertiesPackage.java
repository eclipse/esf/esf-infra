/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
/**
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */
package org.polarsys.esf.esfproperties;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfproperties.IESFPropertiesFactory
 * @model kind="package"
 * annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='ESFProperties'"
 * @generated
 */
public interface IESFPropertiesPackage
    extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "esfproperties"; //$NON-NLS-1$

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.polarsys.org/esf/0.7.0/ESFProperties"; //$NON-NLS-1$

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "ESFProperties"; //$NON-NLS-1$

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    IESFPropertiesPackage eINSTANCE = org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage.init();

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.AbstractSProperty
     * <em>Abstract SProperty</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.AbstractSProperty
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getAbstractSProperty()
     * @generated
     */
    int ABSTRACT_SPROPERTY = 0;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SPROPERTY__UUID = IESFCorePackage.ABSTRACT_SELEMENT__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SPROPERTY__NAME = IESFCorePackage.ABSTRACT_SELEMENT__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SPROPERTY__DESCRIPTION = IESFCorePackage.ABSTRACT_SELEMENT__DESCRIPTION;

    /**
     * The number of structural features of the '<em>Abstract SProperty</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SPROPERTY_FEATURE_COUNT = IESFCorePackage.ABSTRACT_SELEMENT_FEATURE_COUNT + 0;

    /**
     * The number of operations of the '<em>Abstract SProperty</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SPROPERTY_OPERATION_COUNT = IESFCorePackage.ABSTRACT_SELEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.SCriticality <em>SCriticality</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.SCriticality
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSCriticality()
     * @generated
     */
    int SCRITICALITY = 1;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCRITICALITY__UUID = ABSTRACT_SPROPERTY__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCRITICALITY__NAME = ABSTRACT_SPROPERTY__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCRITICALITY__DESCRIPTION = ABSTRACT_SPROPERTY__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCRITICALITY__BASE_DATA_TYPE = ABSTRACT_SPROPERTY_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SCriticality</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCRITICALITY_FEATURE_COUNT = ABSTRACT_SPROPERTY_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SCriticality</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCRITICALITY_OPERATION_COUNT = ABSTRACT_SPROPERTY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.SSeverity <em>SSeverity</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.SSeverity
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSSeverity()
     * @generated
     */
    int SSEVERITY = 2;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSEVERITY__UUID = ABSTRACT_SPROPERTY__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSEVERITY__NAME = ABSTRACT_SPROPERTY__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSEVERITY__DESCRIPTION = ABSTRACT_SPROPERTY__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSEVERITY__BASE_DATA_TYPE = ABSTRACT_SPROPERTY_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SSeverity</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSEVERITY_FEATURE_COUNT = ABSTRACT_SPROPERTY_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SSeverity</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SSEVERITY_OPERATION_COUNT = ABSTRACT_SPROPERTY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.SOccurence <em>SOccurence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.SOccurence
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSOccurence()
     * @generated
     */
    int SOCCURENCE = 3;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOCCURENCE__UUID = ABSTRACT_SPROPERTY__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOCCURENCE__NAME = ABSTRACT_SPROPERTY__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOCCURENCE__DESCRIPTION = ABSTRACT_SPROPERTY__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOCCURENCE__BASE_DATA_TYPE = ABSTRACT_SPROPERTY_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SOccurence</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOCCURENCE_FEATURE_COUNT = ABSTRACT_SPROPERTY_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SOccurence</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOCCURENCE_OPERATION_COUNT = ABSTRACT_SPROPERTY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.SDetectability <em>SDetectability</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.SDetectability
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSDetectability()
     * @generated
     */
    int SDETECTABILITY = 4;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTABILITY__UUID = ABSTRACT_SPROPERTY__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTABILITY__NAME = ABSTRACT_SPROPERTY__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTABILITY__DESCRIPTION = ABSTRACT_SPROPERTY__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTABILITY__BASE_DATA_TYPE = ABSTRACT_SPROPERTY_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SDetectability</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTABILITY_FEATURE_COUNT = ABSTRACT_SPROPERTY_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SDetectability</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SDETECTABILITY_OPERATION_COUNT = ABSTRACT_SPROPERTY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.SProbability <em>SProbability</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.SProbability
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSProbability()
     * @generated
     */
    int SPROBABILITY = 5;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SPROBABILITY__UUID = ABSTRACT_SPROPERTY__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SPROBABILITY__NAME = ABSTRACT_SPROPERTY__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SPROBABILITY__DESCRIPTION = ABSTRACT_SPROPERTY__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SPROBABILITY__BASE_DATA_TYPE = ABSTRACT_SPROPERTY_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SProbability</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SPROBABILITY_FEATURE_COUNT = ABSTRACT_SPROPERTY_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SProbability</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SPROBABILITY_OPERATION_COUNT = ABSTRACT_SPROPERTY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link org.polarsys.esf.esfproperties.impl.SCost <em>SCost</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.polarsys.esf.esfproperties.impl.SCost
     * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSCost()
     * @generated
     */
    int SCOST = 6;

    /**
     * The feature id for the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCOST__UUID = ABSTRACT_SPROPERTY__UUID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCOST__NAME = ABSTRACT_SPROPERTY__NAME;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCOST__DESCRIPTION = ABSTRACT_SPROPERTY__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCOST__BASE_DATA_TYPE = ABSTRACT_SPROPERTY_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>SCost</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCOST_FEATURE_COUNT = ABSTRACT_SPROPERTY_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>SCost</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SCOST_OPERATION_COUNT = ABSTRACT_SPROPERTY_OPERATION_COUNT + 0;

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.IAbstractSProperty
     * <em>Abstract SProperty</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract SProperty</em>'.
     * @see org.polarsys.esf.esfproperties.IAbstractSProperty
     * @generated
     */
    EClass getAbstractSProperty();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.ISCriticality <em>SCriticality</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SCriticality</em>'.
     * @see org.polarsys.esf.esfproperties.ISCriticality
     * @generated
     */
    EClass getSCriticality();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfproperties.ISCriticality#getBase_DataType
     * <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Data Type</em>'.
     * @see org.polarsys.esf.esfproperties.ISCriticality#getBase_DataType()
     * @see #getSCriticality()
     * @generated
     */
    EReference getSCriticality_Base_DataType();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.ISSeverity <em>SSeverity</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SSeverity</em>'.
     * @see org.polarsys.esf.esfproperties.ISSeverity
     * @generated
     */
    EClass getSSeverity();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfproperties.ISSeverity#getBase_DataType
     * <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Data Type</em>'.
     * @see org.polarsys.esf.esfproperties.ISSeverity#getBase_DataType()
     * @see #getSSeverity()
     * @generated
     */
    EReference getSSeverity_Base_DataType();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.ISOccurence <em>SOccurence</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SOccurence</em>'.
     * @see org.polarsys.esf.esfproperties.ISOccurence
     * @generated
     */
    EClass getSOccurence();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfproperties.ISOccurence#getBase_DataType
     * <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Data Type</em>'.
     * @see org.polarsys.esf.esfproperties.ISOccurence#getBase_DataType()
     * @see #getSOccurence()
     * @generated
     */
    EReference getSOccurence_Base_DataType();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.ISDetectability <em>SDetectability</em>}
     * '.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SDetectability</em>'.
     * @see org.polarsys.esf.esfproperties.ISDetectability
     * @generated
     */
    EClass getSDetectability();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfproperties.ISDetectability#getBase_DataType
     * <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Data Type</em>'.
     * @see org.polarsys.esf.esfproperties.ISDetectability#getBase_DataType()
     * @see #getSDetectability()
     * @generated
     */
    EReference getSDetectability_Base_DataType();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.ISProbability <em>SProbability</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SProbability</em>'.
     * @see org.polarsys.esf.esfproperties.ISProbability
     * @generated
     */
    EClass getSProbability();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfproperties.ISProbability#getBase_DataType
     * <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Data Type</em>'.
     * @see org.polarsys.esf.esfproperties.ISProbability#getBase_DataType()
     * @see #getSProbability()
     * @generated
     */
    EReference getSProbability_Base_DataType();

    /**
     * Returns the meta object for class '{@link org.polarsys.esf.esfproperties.ISCost <em>SCost</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>SCost</em>'.
     * @see org.polarsys.esf.esfproperties.ISCost
     * @generated
     */
    EClass getSCost();

    /**
     * Returns the meta object for the reference '{@link org.polarsys.esf.esfproperties.ISCost#getBase_DataType
     * <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Base Data Type</em>'.
     * @see org.polarsys.esf.esfproperties.ISCost#getBase_DataType()
     * @see #getSCost()
     * @generated
     */
    EReference getSCost_Base_DataType();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    IESFPropertiesFactory getESFPropertiesFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.AbstractSProperty
         * <em>Abstract SProperty</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.AbstractSProperty
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getAbstractSProperty()
         * @generated
         */
        EClass ABSTRACT_SPROPERTY = eINSTANCE.getAbstractSProperty();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.SCriticality
         * <em>SCriticality</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.SCriticality
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSCriticality()
         * @generated
         */
        EClass SCRITICALITY = eINSTANCE.getSCriticality();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCRITICALITY__BASE_DATA_TYPE = eINSTANCE.getSCriticality_Base_DataType();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.SSeverity <em>SSeverity</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.SSeverity
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSSeverity()
         * @generated
         */
        EClass SSEVERITY = eINSTANCE.getSSeverity();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SSEVERITY__BASE_DATA_TYPE = eINSTANCE.getSSeverity_Base_DataType();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.SOccurence <em>SOccurence</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.SOccurence
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSOccurence()
         * @generated
         */
        EClass SOCCURENCE = eINSTANCE.getSOccurence();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SOCCURENCE__BASE_DATA_TYPE = eINSTANCE.getSOccurence_Base_DataType();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.SDetectability
         * <em>SDetectability</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.SDetectability
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSDetectability()
         * @generated
         */
        EClass SDETECTABILITY = eINSTANCE.getSDetectability();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SDETECTABILITY__BASE_DATA_TYPE = eINSTANCE.getSDetectability_Base_DataType();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.SProbability
         * <em>SProbability</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.SProbability
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSProbability()
         * @generated
         */
        EClass SPROBABILITY = eINSTANCE.getSProbability();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SPROBABILITY__BASE_DATA_TYPE = eINSTANCE.getSProbability_Base_DataType();

        /**
         * The meta object literal for the '{@link org.polarsys.esf.esfproperties.impl.SCost <em>SCost</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see org.polarsys.esf.esfproperties.impl.SCost
         * @see org.polarsys.esf.esfproperties.impl.ESFPropertiesPackage#getSCost()
         * @generated
         */
        EClass SCOST = eINSTANCE.getSCost();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SCOST__BASE_DATA_TYPE = eINSTANCE.getSCost_Base_DataType();

    }

} // IESFPropertiesPackage
