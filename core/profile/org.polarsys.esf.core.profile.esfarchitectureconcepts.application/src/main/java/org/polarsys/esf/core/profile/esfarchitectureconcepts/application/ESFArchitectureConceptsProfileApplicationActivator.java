/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts.application;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ESFArchitectureConceptsProfileApplicationActivator
    extends EMFPlugin
    implements BundleActivator {

    /** The plug-in ID. */
    public static final String PLUGIN_ID = 
        "org.polarsys.esf.core.profile.esfarchitectureconcepts.application"; //$NON-NLS-1$

    /** Keep track of the encapsulating singleton. */
    public static final ESFArchitectureConceptsProfileApplicationActivator INSTANCE =
        new ESFArchitectureConceptsProfileApplicationActivator();

    /** Execution context for the bundle. */
    private static BundleContext sBundleContext = null;

    /** Keep track of the implementation singleton. */
    private static Implementation sPlugin = null;

    /**
     * Create the instance.
     */
    public ESFArchitectureConceptsProfileApplicationActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * Get BundlexContext.
     *
     * @return BundleContext
     */
    static BundleContext getContext() {
        return sBundleContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(final BundleContext pContext) throws Exception {
        sBundleContext = pContext;

        // Create the plugin implementation manually
        sPlugin = new Implementation();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop(final BundleContext pContext) throws Exception {
        sBundleContext = null;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     *
     * @return The singleton instance
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     *
     * @return The singleton instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * Create an Error status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logError(final String pMessage, final Exception pException) {
        // Create the Error status
        IStatus vStatus = new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create a Warning status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logWarning(final String pMessage, final Exception pException) {
        // Create the Warning status
        IStatus vStatus = new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create an Info status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logInfo(final String pMessage, final Exception pException) {
        // Create the Info status
        IStatus vStatus = new Status(IStatus.INFO, PLUGIN_ID, IStatus.INFO, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * The actual implementation of the Eclipse <b>UIPlugin</b>.
     */
    public static class Implementation
        extends EclipsePlugin {

        /**
         * Creates an instance.
         */
        public Implementation() {
            super();

            // Remember of the static instance
            sPlugin = this;
        }
    }
}
