/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts.application.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.profile.esfarchitectureconcepts.application.ApplyESFArchitectureConceptsAnnotation;
import org.polarsys.esf.core.utils.ModelUtil;

/**
 * Handler class for applying of ESFArchitectureConcepts Annotation.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class ApplyESFArchitectureConceptsAnnotationHandler
    extends AbstractHandler {

    /**
     * Default constructor.
     */
    public ApplyESFArchitectureConceptsAnnotationHandler() {
    }

    /**
     * Get the selected model and apply ESFArchitectureConcepts Annotation.
     *
     * {@inheritDoc}
     */
    @Override
    public Object execute(final ExecutionEvent pEvent) throws ExecutionException {
        ISelection vSelection = HandlerUtil.getCurrentSelection(pEvent);

        final Element vSelectedElement = (Element)
        		ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getElement());

        final Model vSelectedModel = vSelectedElement.getModel();
      
        if (vSelectedModel != null) {
            ApplyESFArchitectureConceptsAnnotation.applyAnnotationOnUMLModel(vSelectedModel);
        }
        return null;
    }
}
