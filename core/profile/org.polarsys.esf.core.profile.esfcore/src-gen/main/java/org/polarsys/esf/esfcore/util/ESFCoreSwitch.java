/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.polarsys.esf.esfcore.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfcore.IESFCorePackage
 * @generated
 */
public class ESFCoreSwitch<T>
    extends Switch<T> {

    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static IESFCorePackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ESFCoreSwitch() {
        if (modelPackage == null) {
            modelPackage = IESFCorePackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
     * result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case IESFCorePackage.ABSTRACT_SELEMENT: {
                IAbstractSElement abstractSElement = (IAbstractSElement) theEObject;
                T result = caseAbstractSElement(abstractSElement);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFCorePackage.ABSTRACT_SARCHITECTURE_ELEMENT: {
                IAbstractSArchitectureElement abstractSArchitectureElement = (IAbstractSArchitectureElement) theEObject;
                T result = caseAbstractSArchitectureElement(abstractSArchitectureElement);
                if (result == null)
                    result = caseAbstractSElement(abstractSArchitectureElement);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFCorePackage.ABSTRACT_SSAFETY_CONCEPT: {
                IAbstractSSafetyConcept abstractSSafetyConcept = (IAbstractSSafetyConcept) theEObject;
                T result = caseAbstractSSafetyConcept(abstractSSafetyConcept);
                if (result == null)
                    result = caseAbstractSElement(abstractSSafetyConcept);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS: {
                IAbstractSSafetyAnalysis abstractSSafetyAnalysis = (IAbstractSSafetyAnalysis) theEObject;
                T result = caseAbstractSSafetyAnalysis(abstractSSafetyAnalysis);
                if (result == null)
                    result = caseAbstractSElement(abstractSSafetyAnalysis);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case IESFCorePackage.ABSTRACT_SREQUIREMENT: {
                IAbstractSRequirement abstractSRequirement = (IAbstractSRequirement) theEObject;
                T result = caseAbstractSRequirement(abstractSRequirement);
                if (result == null)
                    result = caseAbstractSElement(abstractSRequirement);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            default:
                return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSElement(IAbstractSElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SArchitecture Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SArchitecture Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSArchitectureElement(IAbstractSArchitectureElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Analysis</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Analysis</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSSafetyAnalysis(IAbstractSSafetyAnalysis object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SRequirement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SRequirement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSRequirement(IAbstractSRequirement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} // ESFCoreSwitch
