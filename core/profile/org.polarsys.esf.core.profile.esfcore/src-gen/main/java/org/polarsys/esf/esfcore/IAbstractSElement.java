/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract SElement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfcore.IAbstractSElement#getUUID <em>UUID</em>}</li>
 * <li>{@link org.polarsys.esf.esfcore.IAbstractSElement#getName <em>Name</em>}</li>
 * <li>{@link org.polarsys.esf.esfcore.IAbstractSElement#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSElement()
 * @model abstract="true"
 * @generated
 */
public interface IAbstractSElement
    extends EObject {

    /**
     * Returns the value of the '<em><b>UUID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>UUID</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>UUID</em>' attribute.
     * @see #setUUID(String)
     * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSElement_UUID()
     * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
     * @generated
     */
    String getUUID();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfcore.IAbstractSElement#getUUID <em>UUID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>UUID</em>' attribute.
     * @see #getUUID()
     * @generated
     */
    void setUUID(String value);

    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSElement_Name()
     * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfcore.IAbstractSElement#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see org.polarsys.esf.esfcore.IESFCorePackage#getAbstractSElement_Description()
     * @model unique="false" dataType="org.eclipse.uml2.types.String" ordered="false"
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfcore.IAbstractSElement#getDescription <em>Description</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

} // IAbstractSElement
