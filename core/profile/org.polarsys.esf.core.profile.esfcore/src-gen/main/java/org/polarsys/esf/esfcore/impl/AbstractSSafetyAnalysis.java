/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfcore.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.polarsys.esf.esfcore.IAbstractSSafetyAnalysis;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;
import org.polarsys.esf.esfcore.IESFCorePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract SSafety Analysis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfcore.impl.AbstractSSafetyAnalysis#getSSafetyConceptsList <em>SSafety Concepts
 * List</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSSafetyAnalysis
    extends AbstractSElement
    implements IAbstractSSafetyAnalysis {

    /**
     * The cached value of the '{@link #getSSafetyConceptsList() <em>SSafety Concepts List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSSafetyConceptsList()
     * @generated
     * @ordered
     */
    protected EList<IAbstractSSafetyConcept> sSafetyConceptsList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractSSafetyAnalysis() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IESFCorePackage.Literals.ABSTRACT_SSAFETY_ANALYSIS;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<IAbstractSSafetyConcept> getSSafetyConceptsList() {
        if (sSafetyConceptsList == null) {
            sSafetyConceptsList = new EObjectResolvingEList<IAbstractSSafetyConcept>(
                IAbstractSSafetyConcept.class,
                this,
                IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST);
        }
        return sSafetyConceptsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST:
                return getSSafetyConceptsList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST:
                getSSafetyConceptsList().clear();
                getSSafetyConceptsList().addAll((Collection<? extends IAbstractSSafetyConcept>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST:
                getSSafetyConceptsList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case IESFCorePackage.ABSTRACT_SSAFETY_ANALYSIS__SSAFETY_CONCEPTS_LIST:
                return sSafetyConceptsList != null && !sSafetyConceptsList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // AbstractSSafetyAnalysis
