/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/

package org.polarsys.esf.core.profile.esfcore;

import org.eclipse.uml2.uml.Element;

/**
 * Enable customization of UUIDs
 */
public interface ICustomUUID {
	/**
	 * @param element
	 *            a UML element
	 * @return true, if ESF should show the calculated UUID (which might be
	 *         specialized by the extension, see below)
	 */
	boolean showUUID(Element element);

	/**
	 * @param element
	 *            a UML element
	 * @return the UUID that is calculated by the extension. A null value implies
	 *         that the customization/extension could not calculate an UUID from
	 *         the passed element
	 */
	String getUUID(Element element);
}
