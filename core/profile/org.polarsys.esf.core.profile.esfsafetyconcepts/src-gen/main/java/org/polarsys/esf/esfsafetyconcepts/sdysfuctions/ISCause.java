/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.eclipse.emf.common.util.EList;
import org.polarsys.esf.esfproperties.ISDetectability;
import org.polarsys.esf.esfproperties.ISOccurence;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SCause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getFailureEventsList <em>Failure Events
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getOccurence <em>Occurence</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getDetectability <em>Detectability</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getRisksList <em>Risks List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSCause()
 * @model
 * @generated
 */
public interface ISCause
    extends IAbstractSDysfunctionObject {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSCause_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getBase_Class <em>Base
     * Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>Failure Events List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCausesList <em>Causes List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Events List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Failure Events List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSCause_FailureEventsList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCausesList
     * @model opposite="causesList" required="true" ordered="false"
     * @generated
     */
    EList<ISFailureEvent> getFailureEventsList();

    /**
     * Returns the value of the '<em><b>Occurence</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurence</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Occurence</em>' reference.
     * @see #setOccurence(ISOccurence)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSCause_Occurence()
     * @model required="true" ordered="false"
     * @generated
     */
    ISOccurence getOccurence();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getOccurence
     * <em>Occurence</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Occurence</em>' reference.
     * @see #getOccurence()
     * @generated
     */
    void setOccurence(ISOccurence value);

    /**
     * Returns the value of the '<em><b>Detectability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Detectability</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Detectability</em>' reference.
     * @see #setDetectability(ISDetectability)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSCause_Detectability()
     * @model required="true" ordered="false"
     * @generated
     */
    ISDetectability getDetectability();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getDetectability
     * <em>Detectability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Detectability</em>' reference.
     * @see #getDetectability()
     * @generated
     */
    void setDetectability(ISDetectability value);

    /**
     * Returns the value of the '<em><b>Risks List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getCausesList <em>Causes List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Risks List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Risks List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSCause_RisksList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getCausesList
     * @model opposite="causesList" ordered="false"
     * @generated
     */
    EList<ISRisk> getRisksList();

} // ISCause
