/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SBarrier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getSActionsList <em>SActions List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSBarrier()
 * @model
 * @generated
 */
public interface ISBarrier
    extends ISRecommendation {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSBarrier_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier#getBase_Class
     * <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>SActions List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getSBarriersList <em>SBarriers List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>SActions List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>SActions List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#getSBarrier_SActionsList()
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction#getSBarriersList
     * @model opposite="sBarriersList" ordered="false"
     * @generated
     */
    EList<ISAction> getSActionsList();

} // ISBarrier
