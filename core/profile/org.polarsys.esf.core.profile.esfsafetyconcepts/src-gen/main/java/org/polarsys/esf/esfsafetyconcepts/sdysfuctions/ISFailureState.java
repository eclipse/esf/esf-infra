/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SFailure State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getFailureMode <em>Failure Mode</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureState()
 * @model
 * @generated
 */
public interface ISFailureState
    extends IAbstractSDysfunctionObject {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureState_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getBase_Class
     * <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>Failure Mode</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Mode</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Failure Mode</em>' reference.
     * @see #setFailureMode(ISFailureMode)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureState_FailureMode()
     * @model ordered="false"
     * @generated
     */
    ISFailureMode getFailureMode();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureState#getFailureMode
     * <em>Failure Mode</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Failure Mode</em>' reference.
     * @see #getFailureMode()
     * @generated
     */
    void setFailureMode(ISFailureMode value);

} // ISFailureState
