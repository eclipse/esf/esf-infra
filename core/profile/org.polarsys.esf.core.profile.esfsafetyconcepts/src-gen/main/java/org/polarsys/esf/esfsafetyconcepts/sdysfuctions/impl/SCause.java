/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.polarsys.esf.esfproperties.ISDetectability;
import org.polarsys.esf.esfproperties.ISOccurence;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SCause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause#getFailureEventsList <em>Failure Events
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause#getOccurence <em>Occurence</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause#getDetectability <em>Detectability</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SCause#getRisksList <em>Risks List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SCause
    extends AbstractSDysfunctionObject
    implements ISCause {

    /**
     * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Class()
     * @generated
     * @ordered
     */
    protected org.eclipse.uml2.uml.Class base_Class;

    /**
     * The cached value of the '{@link #getFailureEventsList() <em>Failure Events List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFailureEventsList()
     * @generated
     * @ordered
     */
    protected EList<ISFailureEvent> failureEventsList;

    /**
     * The cached value of the '{@link #getOccurence() <em>Occurence</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOccurence()
     * @generated
     * @ordered
     */
    protected ISOccurence occurence;

    /**
     * The cached value of the '{@link #getDetectability() <em>Detectability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDetectability()
     * @generated
     * @ordered
     */
    protected ISDetectability detectability;

    /**
     * The cached value of the '{@link #getRisksList() <em>Risks List</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRisksList()
     * @generated
     * @ordered
     */
    protected EList<ISRisk> risksList;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SCause() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISDysfunctionsPackage.Literals.SCAUSE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class getBase_Class() {
        if (base_Class != null && base_Class.eIsProxy()) {
            InternalEObject oldBase_Class = (InternalEObject) base_Class;
            base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
            if (base_Class != oldBase_Class) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SCAUSE__BASE_CLASS,
                            oldBase_Class,
                            base_Class));
            }
        }
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class basicGetBase_Class() {
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
        org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
        base_Class = newBase_Class;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SCAUSE__BASE_CLASS,
                    oldBase_Class,
                    base_Class));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISFailureEvent> getFailureEventsList() {
        if (failureEventsList == null) {
            failureEventsList = new EObjectWithInverseResolvingEList.ManyInverse<ISFailureEvent>(
                ISFailureEvent.class,
                this,
                ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST,
                ISDysfunctionsPackage.SFAILURE_EVENT__CAUSES_LIST);
        }
        return failureEventsList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISOccurence getOccurence() {
        if (occurence != null && occurence.eIsProxy()) {
            InternalEObject oldOccurence = (InternalEObject) occurence;
            occurence = (ISOccurence) eResolveProxy(oldOccurence);
            if (occurence != oldOccurence) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SCAUSE__OCCURENCE,
                            oldOccurence,
                            occurence));
            }
        }
        return occurence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISOccurence basicGetOccurence() {
        return occurence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setOccurence(ISOccurence newOccurence) {
        ISOccurence oldOccurence = occurence;
        occurence = newOccurence;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SCAUSE__OCCURENCE,
                    oldOccurence,
                    occurence));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDetectability getDetectability() {
        if (detectability != null && detectability.eIsProxy()) {
            InternalEObject oldDetectability = (InternalEObject) detectability;
            detectability = (ISDetectability) eResolveProxy(oldDetectability);
            if (detectability != oldDetectability) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SCAUSE__DETECTABILITY,
                            oldDetectability,
                            detectability));
            }
        }
        return detectability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISDetectability basicGetDetectability() {
        return detectability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setDetectability(ISDetectability newDetectability) {
        ISDetectability oldDetectability = detectability;
        detectability = newDetectability;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SCAUSE__DETECTABILITY,
                    oldDetectability,
                    detectability));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EList<ISRisk> getRisksList() {
        if (risksList == null) {
            risksList = new EObjectWithInverseResolvingEList.ManyInverse<ISRisk>(
                ISRisk.class,
                this,
                ISDysfunctionsPackage.SCAUSE__RISKS_LIST,
                ISDysfunctionsPackage.SRISK__CAUSES_LIST);
        }
        return risksList;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getFailureEventsList())
                    .basicAdd(otherEnd, msgs);
            case ISDysfunctionsPackage.SCAUSE__RISKS_LIST:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getRisksList()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST:
                return ((InternalEList<?>) getFailureEventsList()).basicRemove(otherEnd, msgs);
            case ISDysfunctionsPackage.SCAUSE__RISKS_LIST:
                return ((InternalEList<?>) getRisksList()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISDysfunctionsPackage.SCAUSE__BASE_CLASS:
                if (resolve)
                    return getBase_Class();
                return basicGetBase_Class();
            case ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST:
                return getFailureEventsList();
            case ISDysfunctionsPackage.SCAUSE__OCCURENCE:
                if (resolve)
                    return getOccurence();
                return basicGetOccurence();
            case ISDysfunctionsPackage.SCAUSE__DETECTABILITY:
                if (resolve)
                    return getDetectability();
                return basicGetDetectability();
            case ISDysfunctionsPackage.SCAUSE__RISKS_LIST:
                return getRisksList();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISDysfunctionsPackage.SCAUSE__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) newValue);
                return;
            case ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST:
                getFailureEventsList().clear();
                getFailureEventsList().addAll((Collection<? extends ISFailureEvent>) newValue);
                return;
            case ISDysfunctionsPackage.SCAUSE__OCCURENCE:
                setOccurence((ISOccurence) newValue);
                return;
            case ISDysfunctionsPackage.SCAUSE__DETECTABILITY:
                setDetectability((ISDetectability) newValue);
                return;
            case ISDysfunctionsPackage.SCAUSE__RISKS_LIST:
                getRisksList().clear();
                getRisksList().addAll((Collection<? extends ISRisk>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SCAUSE__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) null);
                return;
            case ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST:
                getFailureEventsList().clear();
                return;
            case ISDysfunctionsPackage.SCAUSE__OCCURENCE:
                setOccurence((ISOccurence) null);
                return;
            case ISDysfunctionsPackage.SCAUSE__DETECTABILITY:
                setDetectability((ISDetectability) null);
                return;
            case ISDysfunctionsPackage.SCAUSE__RISKS_LIST:
                getRisksList().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SCAUSE__BASE_CLASS:
                return base_Class != null;
            case ISDysfunctionsPackage.SCAUSE__FAILURE_EVENTS_LIST:
                return failureEventsList != null && !failureEventsList.isEmpty();
            case ISDysfunctionsPackage.SCAUSE__OCCURENCE:
                return occurence != null;
            case ISDysfunctionsPackage.SCAUSE__DETECTABILITY:
                return detectability != null;
            case ISDysfunctionsPackage.SCAUSE__RISKS_LIST:
                return risksList != null && !risksList.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} // SCause
