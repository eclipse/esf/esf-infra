/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.esfcore.IESFCorePackage;
import org.polarsys.esf.esfproperties.IESFPropertiesPackage;
import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsPackage;
import org.polarsys.esf.esfsafetyconcepts.impl.ESFSafetyConceptsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsFactory;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.SActionsKind;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class SRecommendationsPackage
    extends EPackageImpl
    implements ISRecommendationsPackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sBarrierEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sRecommendationEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sDetectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum sActionsKindEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private SRecommendationsPackage() {
        super(eNS_URI, ISRecommendationsFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>
     * This method is used to initialize {@link ISRecommendationsPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static ISRecommendationsPackage init() {
        if (isInited)
            return (ISRecommendationsPackage) EPackage.Registry.INSTANCE.getEPackage(ISRecommendationsPackage.eNS_URI);

        // Obtain or create and register package
        SRecommendationsPackage theSRecommendationsPackage =
            (SRecommendationsPackage) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SRecommendationsPackage
                ? EPackage.Registry.INSTANCE.get(eNS_URI)
                : new SRecommendationsPackage());

        isInited = true;

        // Initialize simple dependencies
        IESFCorePackage.eINSTANCE.eClass();
        IESFPropertiesPackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        ESFSafetyConceptsPackage theESFSafetyConceptsPackage = (ESFSafetyConceptsPackage) (EPackage.Registry.INSTANCE
            .getEPackage(IESFSafetyConceptsPackage.eNS_URI) instanceof ESFSafetyConceptsPackage
                ? EPackage.Registry.INSTANCE.getEPackage(IESFSafetyConceptsPackage.eNS_URI)
                : IESFSafetyConceptsPackage.eINSTANCE);
        SDysfunctionsPackage theSDysfunctionsPackage = (SDysfunctionsPackage) (EPackage.Registry.INSTANCE
            .getEPackage(ISDysfunctionsPackage.eNS_URI) instanceof SDysfunctionsPackage
                ? EPackage.Registry.INSTANCE.getEPackage(ISDysfunctionsPackage.eNS_URI)
                : ISDysfunctionsPackage.eINSTANCE);

        // Create package meta-data objects
        theSRecommendationsPackage.createPackageContents();
        theESFSafetyConceptsPackage.createPackageContents();
        theSDysfunctionsPackage.createPackageContents();

        // Initialize created meta-data
        theSRecommendationsPackage.initializePackageContents();
        theESFSafetyConceptsPackage.initializePackageContents();
        theSDysfunctionsPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theSRecommendationsPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ISRecommendationsPackage.eNS_URI, theSRecommendationsPackage);
        return theSRecommendationsPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSBarrier() {
        return sBarrierEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSBarrier_Base_Class() {
        return (EReference) sBarrierEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSBarrier_SActionsList() {
        return (EReference) sBarrierEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSRecommendation() {
        return sRecommendationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSRecommendation_Base_Classifier() {
        return (EReference) sRecommendationEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSAction() {
        return sActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EAttribute getSAction_Kind() {
        return (EAttribute) sActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSAction_SBarriersList() {
        return (EReference) sActionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EClass getSDetection() {
        return sDetectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EReference getSDetection_Base_Classifier() {
        return (EReference) sDetectionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public EEnum getSActionsKind() {
        return sActionsKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISRecommendationsFactory getSRecommendationsFactory() {
        return (ISRecommendationsFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        sBarrierEClass = createEClass(SBARRIER);
        createEReference(sBarrierEClass, SBARRIER__BASE_CLASS);
        createEReference(sBarrierEClass, SBARRIER__SACTIONS_LIST);

        sRecommendationEClass = createEClass(SRECOMMENDATION);
        createEReference(sRecommendationEClass, SRECOMMENDATION__BASE_CLASSIFIER);

        sActionEClass = createEClass(SACTION);
        createEAttribute(sActionEClass, SACTION__KIND);
        createEReference(sActionEClass, SACTION__SBARRIERS_LIST);

        sDetectionEClass = createEClass(SDETECTION);
        createEReference(sDetectionEClass, SDETECTION__BASE_CLASSIFIER);

        // Create enums
        sActionsKindEEnum = createEEnum(SACTIONS_KIND);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
        IESFCorePackage theESFCorePackage =
            (IESFCorePackage) EPackage.Registry.INSTANCE.getEPackage(IESFCorePackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        sBarrierEClass.getESuperTypes().add(this.getSRecommendation());
        sRecommendationEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyConcept());
        sActionEClass.getESuperTypes().add(this.getSRecommendation());
        sDetectionEClass.getESuperTypes().add(theESFCorePackage.getAbstractSSafetyConcept());

        // Initialize classes, features, and operations; add parameters
        initEClass(
            sBarrierEClass,
            ISBarrier.class,
            "SBarrier", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSBarrier_Base_Class(),
            theUMLPackage.getClass_(),
            null,
            "base_Class", //$NON-NLS-1$
            null,
            1,
            1,
            ISBarrier.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSBarrier_SActionsList(),
            this.getSAction(),
            this.getSAction_SBarriersList(),
            "sActionsList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISBarrier.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sRecommendationEClass,
            ISRecommendation.class,
            "SRecommendation", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSRecommendation_Base_Classifier(),
            theUMLPackage.getClassifier(),
            null,
            "base_Classifier", //$NON-NLS-1$
            null,
            1,
            1,
            ISRecommendation.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(sActionEClass, ISAction.class, "SAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
        initEAttribute(
            getSAction_Kind(),
            this.getSActionsKind(),
            "kind", //$NON-NLS-1$
            null,
            1,
            1,
            ISAction.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_UNSETTABLE,
            !IS_ID,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);
        initEReference(
            getSAction_SBarriersList(),
            this.getSBarrier(),
            this.getSBarrier_SActionsList(),
            "sBarriersList", //$NON-NLS-1$
            null,
            0,
            -1,
            ISAction.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        initEClass(
            sDetectionEClass,
            ISDetection.class,
            "SDetection", //$NON-NLS-1$
            !IS_ABSTRACT,
            !IS_INTERFACE,
            IS_GENERATED_INSTANCE_CLASS);
        initEReference(
            getSDetection_Base_Classifier(),
            theUMLPackage.getClassifier(),
            null,
            "base_Classifier", //$NON-NLS-1$
            null,
            1,
            1,
            ISDetection.class,
            !IS_TRANSIENT,
            !IS_VOLATILE,
            IS_CHANGEABLE,
            !IS_COMPOSITE,
            IS_RESOLVE_PROXIES,
            !IS_UNSETTABLE,
            IS_UNIQUE,
            !IS_DERIVED,
            !IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(sActionsKindEEnum, SActionsKind.class, "SActionsKind"); //$NON-NLS-1$
        addEEnumLiteral(sActionsKindEEnum, SActionsKind.PREVENTIVE_ACTION);
        addEEnumLiteral(sActionsKindEEnum, SActionsKind.SCORRECTIVE_ACTION);

        // Create annotations
        // http://www.eclipse.org/uml2/2.0.0/UML
        createUMLAnnotations();
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createUMLAnnotations() {
        String source = "http://www.eclipse.org/uml2/2.0.0/UML"; //$NON-NLS-1$
        addAnnotation(this, source, new String[] {"originalName", "SRecommendations" //$NON-NLS-1$ //$NON-NLS-2$
        });
    }

} // SRecommendationsPackage
