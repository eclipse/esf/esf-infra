/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.srecommendations.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.polarsys.esf.esfcore.IAbstractSElement;
import org.polarsys.esf.esfcore.IAbstractSSafetyConcept;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISAction;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendationsPackage
 * @generated
 */
public class SRecommendationsSwitch<T>
    extends Switch<T> {

    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ISRecommendationsPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public SRecommendationsSwitch() {
        if (modelPackage == null) {
            modelPackage = ISRecommendationsPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that
     * result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case ISRecommendationsPackage.SBARRIER: {
                ISBarrier sBarrier = (ISBarrier) theEObject;
                T result = caseSBarrier(sBarrier);
                if (result == null)
                    result = caseSRecommendation(sBarrier);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sBarrier);
                if (result == null)
                    result = caseAbstractSElement(sBarrier);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISRecommendationsPackage.SRECOMMENDATION: {
                ISRecommendation sRecommendation = (ISRecommendation) theEObject;
                T result = caseSRecommendation(sRecommendation);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sRecommendation);
                if (result == null)
                    result = caseAbstractSElement(sRecommendation);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISRecommendationsPackage.SACTION: {
                ISAction sAction = (ISAction) theEObject;
                T result = caseSAction(sAction);
                if (result == null)
                    result = caseSRecommendation(sAction);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sAction);
                if (result == null)
                    result = caseAbstractSElement(sAction);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            case ISRecommendationsPackage.SDETECTION: {
                ISDetection sDetection = (ISDetection) theEObject;
                T result = caseSDetection(sDetection);
                if (result == null)
                    result = caseAbstractSSafetyConcept(sDetection);
                if (result == null)
                    result = caseAbstractSElement(sDetection);
                if (result == null)
                    result = defaultCase(theEObject);
                return result;
            }
            default:
                return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SBarrier</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SBarrier</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSBarrier(ISBarrier object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SRecommendation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SRecommendation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSRecommendation(ISRecommendation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SAction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SAction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSAction(ISAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>SDetection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>SDetection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSDetection(ISDetection object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSElement(IAbstractSElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract SSafety Concept</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSSafetyConcept(IAbstractSSafetyConcept object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} // SRecommendationsSwitch
