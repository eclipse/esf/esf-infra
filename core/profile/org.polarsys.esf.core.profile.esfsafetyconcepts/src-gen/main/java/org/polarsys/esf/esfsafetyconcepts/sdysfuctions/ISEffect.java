/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.eclipse.emf.common.util.EList;
import org.polarsys.esf.esfproperties.ISCost;
import org.polarsys.esf.esfproperties.ISCriticality;
import org.polarsys.esf.esfproperties.ISSeverity;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SEffect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getRisksList <em>Risks List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getSeverity <em>Severity</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCost <em>Cost</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCriticality <em>Criticality</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getFailureEventsList <em>Failure Events
 * List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect()
 * @model
 * @generated
 */
public interface ISEffect
    extends IAbstractSDysfunctionObject {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getBase_Class <em>Base
     * Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>Risks List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getEffectsList <em>Effects List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Risks List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Risks List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect_RisksList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getEffectsList
     * @model opposite="effectsList" ordered="false"
     * @generated
     */
    EList<ISRisk> getRisksList();

    /**
     * Returns the value of the '<em><b>Severity</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Severity</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Severity</em>' reference.
     * @see #setSeverity(ISSeverity)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect_Severity()
     * @model required="true" ordered="false"
     * @generated
     */
    ISSeverity getSeverity();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getSeverity
     * <em>Severity</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Severity</em>' reference.
     * @see #getSeverity()
     * @generated
     */
    void setSeverity(ISSeverity value);

    /**
     * Returns the value of the '<em><b>Cost</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cost</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Cost</em>' reference.
     * @see #setCost(ISCost)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect_Cost()
     * @model required="true" ordered="false"
     * @generated
     */
    ISCost getCost();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCost <em>Cost</em>}'
     * reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Cost</em>' reference.
     * @see #getCost()
     * @generated
     */
    void setCost(ISCost value);

    /**
     * Returns the value of the '<em><b>Criticality</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Criticality</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Criticality</em>' reference.
     * @see #setCriticality(ISCriticality)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect_Criticality()
     * @model required="true" ordered="false"
     * @generated
     */
    ISCriticality getCriticality();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getCriticality
     * <em>Criticality</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Criticality</em>' reference.
     * @see #getCriticality()
     * @generated
     */
    void setCriticality(ISCriticality value);

    /**
     * Returns the value of the '<em><b>Failure Events List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getEffectsList <em>Effects List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Events List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Failure Events List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSEffect_FailureEventsList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getEffectsList
     * @model opposite="effectsList" ordered="false"
     * @generated
     */
    EList<ISFailureEvent> getFailureEventsList();

} // ISEffect
