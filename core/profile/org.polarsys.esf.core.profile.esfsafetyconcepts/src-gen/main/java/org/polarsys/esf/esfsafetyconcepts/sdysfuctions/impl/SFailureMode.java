/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent;
import org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISBarrier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SFailure Mode</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode#getFailureEvent <em>Failure
 * Event</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.impl.SFailureMode#getBarrier <em>Barrier</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SFailureMode
    extends AbstractSDysfunctionObject
    implements ISFailureMode {

    /**
     * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBase_Class()
     * @generated
     * @ordered
     */
    protected org.eclipse.uml2.uml.Class base_Class;

    /**
     * The cached value of the '{@link #getFailureEvent() <em>Failure Event</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFailureEvent()
     * @generated
     * @ordered
     */
    protected ISFailureEvent failureEvent;

    /**
     * The cached value of the '{@link #getBarrier() <em>Barrier</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBarrier()
     * @generated
     * @ordered
     */
    protected ISBarrier barrier;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SFailureMode() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ISDysfunctionsPackage.Literals.SFAILURE_MODE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class getBase_Class() {
        if (base_Class != null && base_Class.eIsProxy()) {
            InternalEObject oldBase_Class = (InternalEObject) base_Class;
            base_Class = (org.eclipse.uml2.uml.Class) eResolveProxy(oldBase_Class);
            if (base_Class != oldBase_Class) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_MODE__BASE_CLASS,
                            oldBase_Class,
                            base_Class));
            }
        }
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public org.eclipse.uml2.uml.Class basicGetBase_Class() {
        return base_Class;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
        org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
        base_Class = newBase_Class;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_MODE__BASE_CLASS,
                    oldBase_Class,
                    base_Class));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureEvent getFailureEvent() {
        if (failureEvent != null && failureEvent.eIsProxy()) {
            InternalEObject oldFailureEvent = (InternalEObject) failureEvent;
            failureEvent = (ISFailureEvent) eResolveProxy(oldFailureEvent);
            if (failureEvent != oldFailureEvent) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT,
                            oldFailureEvent,
                            failureEvent));
            }
        }
        return failureEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISFailureEvent basicGetFailureEvent() {
        return failureEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetFailureEvent(ISFailureEvent newFailureEvent, NotificationChain msgs) {
        ISFailureEvent oldFailureEvent = failureEvent;
        failureEvent = newFailureEvent;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(
                this,
                Notification.SET,
                ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT,
                oldFailureEvent,
                newFailureEvent);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setFailureEvent(ISFailureEvent newFailureEvent) {
        if (newFailureEvent != failureEvent) {
            NotificationChain msgs = null;
            if (failureEvent != null)
                msgs = ((InternalEObject) failureEvent).eInverseRemove(
                    this,
                    ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE,
                    ISFailureEvent.class,
                    msgs);
            if (newFailureEvent != null)
                msgs = ((InternalEObject) newFailureEvent)
                    .eInverseAdd(this, ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE, ISFailureEvent.class, msgs);
            msgs = basicSetFailureEvent(newFailureEvent, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT,
                    newFailureEvent,
                    newFailureEvent));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISBarrier getBarrier() {
        if (barrier != null && barrier.eIsProxy()) {
            InternalEObject oldBarrier = (InternalEObject) barrier;
            barrier = (ISBarrier) eResolveProxy(oldBarrier);
            if (barrier != oldBarrier) {
                if (eNotificationRequired())
                    eNotify(
                        new ENotificationImpl(
                            this,
                            Notification.RESOLVE,
                            ISDysfunctionsPackage.SFAILURE_MODE__BARRIER,
                            oldBarrier,
                            barrier));
            }
        }
        return barrier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISBarrier basicGetBarrier() {
        return barrier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void setBarrier(ISBarrier newBarrier) {
        ISBarrier oldBarrier = barrier;
        barrier = newBarrier;
        if (eNotificationRequired())
            eNotify(
                new ENotificationImpl(
                    this,
                    Notification.SET,
                    ISDysfunctionsPackage.SFAILURE_MODE__BARRIER,
                    oldBarrier,
                    barrier));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT:
                if (failureEvent != null)
                    msgs = ((InternalEObject) failureEvent).eInverseRemove(
                        this,
                        ISDysfunctionsPackage.SFAILURE_EVENT__FAILURE_MODE,
                        ISFailureEvent.class,
                        msgs);
                return basicSetFailureEvent((ISFailureEvent) otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT:
                return basicSetFailureEvent(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_MODE__BASE_CLASS:
                if (resolve)
                    return getBase_Class();
                return basicGetBase_Class();
            case ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT:
                if (resolve)
                    return getFailureEvent();
                return basicGetFailureEvent();
            case ISDysfunctionsPackage.SFAILURE_MODE__BARRIER:
                if (resolve)
                    return getBarrier();
                return basicGetBarrier();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_MODE__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT:
                setFailureEvent((ISFailureEvent) newValue);
                return;
            case ISDysfunctionsPackage.SFAILURE_MODE__BARRIER:
                setBarrier((ISBarrier) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_MODE__BASE_CLASS:
                setBase_Class((org.eclipse.uml2.uml.Class) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT:
                setFailureEvent((ISFailureEvent) null);
                return;
            case ISDysfunctionsPackage.SFAILURE_MODE__BARRIER:
                setBarrier((ISBarrier) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ISDysfunctionsPackage.SFAILURE_MODE__BASE_CLASS:
                return base_Class != null;
            case ISDysfunctionsPackage.SFAILURE_MODE__FAILURE_EVENT:
                return failureEvent != null;
            case ISDysfunctionsPackage.SFAILURE_MODE__BARRIER:
                return barrier != null;
        }
        return super.eIsSet(featureID);
    }

} // SFailureMode
