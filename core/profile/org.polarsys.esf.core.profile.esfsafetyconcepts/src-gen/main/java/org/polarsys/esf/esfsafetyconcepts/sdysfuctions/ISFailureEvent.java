/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.sdysfuctions;

import org.eclipse.emf.common.util.EList;
import org.polarsys.esf.esfproperties.ISCost;
import org.polarsys.esf.esfproperties.ISCriticality;
import org.polarsys.esf.esfproperties.ISDetectability;
import org.polarsys.esf.esfproperties.ISOccurence;
import org.polarsys.esf.esfproperties.ISProbability;
import org.polarsys.esf.esfproperties.ISSeverity;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection;
import org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SFailure Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getEffectsList <em>Effects List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRisk <em>Risk</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCausesList <em>Causes List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectionsList <em>Detections
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRecommendationsList <em>Recommendations
 * List</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getSeverity <em>Severity</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCriticality <em>Criticality</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCost <em>Cost</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getOccurence <em>Occurence</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectability
 * <em>Detectability</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getProbability <em>Probability</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#isFearedEvent <em>Feared Event</em>}</li>
 * <li>{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getFailureMode <em>Failure Mode</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent()
 * @model
 * @generated
 */
public interface ISFailureEvent
    extends IAbstractSDysfunctionObject {

    /**
     * Returns the value of the '<em><b>Base Class</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Base Class</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Base Class</em>' reference.
     * @see #setBase_Class(org.eclipse.uml2.uml.Class)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Base_Class()
     * @model required="true" ordered="false"
     * @generated
     */
    org.eclipse.uml2.uml.Class getBase_Class();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getBase_Class
     * <em>Base Class</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Base Class</em>' reference.
     * @see #getBase_Class()
     * @generated
     */
    void setBase_Class(org.eclipse.uml2.uml.Class value);

    /**
     * Returns the value of the '<em><b>Effects List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getFailureEventsList <em>Failure Events
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Effects List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Effects List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_EffectsList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISEffect#getFailureEventsList
     * @model opposite="failureEventsList" ordered="false"
     * @generated
     */
    EList<ISEffect> getEffectsList();

    /**
     * Returns the value of the '<em><b>Risk</b></em>' reference.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getFailureEvent <em>Failure Event</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Risk</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Risk</em>' reference.
     * @see #setRisk(ISRisk)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Risk()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISRisk#getFailureEvent
     * @model opposite="failureEvent" ordered="false"
     * @generated
     */
    ISRisk getRisk();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getRisk
     * <em>Risk</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Risk</em>' reference.
     * @see #getRisk()
     * @generated
     */
    void setRisk(ISRisk value);

    /**
     * Returns the value of the '<em><b>Causes List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause}.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getFailureEventsList <em>Failure Events
     * List</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Causes List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Causes List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_CausesList()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISCause#getFailureEventsList
     * @model opposite="failureEventsList" required="true" ordered="false"
     * @generated
     */
    EList<ISCause> getCausesList();

    /**
     * Returns the value of the '<em><b>Detections List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISDetection}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Detections List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Detections List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_DetectionsList()
     * @model required="true" ordered="false"
     * @generated
     */
    EList<ISDetection> getDetectionsList();

    /**
     * Returns the value of the '<em><b>Recommendations List</b></em>' reference list.
     * The list contents are of type {@link org.polarsys.esf.esfsafetyconcepts.srecommendations.ISRecommendation}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Recommendations List</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Recommendations List</em>' reference list.
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_RecommendationsList()
     * @model ordered="false"
     * @generated
     */
    EList<ISRecommendation> getRecommendationsList();

    /**
     * Returns the value of the '<em><b>Severity</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Severity</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Severity</em>' reference.
     * @see #setSeverity(ISSeverity)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Severity()
     * @model required="true" ordered="false"
     * @generated
     */
    ISSeverity getSeverity();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getSeverity
     * <em>Severity</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Severity</em>' reference.
     * @see #getSeverity()
     * @generated
     */
    void setSeverity(ISSeverity value);

    /**
     * Returns the value of the '<em><b>Criticality</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Criticality</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Criticality</em>' reference.
     * @see #setCriticality(ISCriticality)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Criticality()
     * @model required="true" ordered="false"
     * @generated
     */
    ISCriticality getCriticality();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCriticality
     * <em>Criticality</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Criticality</em>' reference.
     * @see #getCriticality()
     * @generated
     */
    void setCriticality(ISCriticality value);

    /**
     * Returns the value of the '<em><b>Cost</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cost</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Cost</em>' reference.
     * @see #setCost(ISCost)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Cost()
     * @model required="true" ordered="false"
     * @generated
     */
    ISCost getCost();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getCost
     * <em>Cost</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Cost</em>' reference.
     * @see #getCost()
     * @generated
     */
    void setCost(ISCost value);

    /**
     * Returns the value of the '<em><b>Occurence</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Occurence</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Occurence</em>' reference.
     * @see #setOccurence(ISOccurence)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Occurence()
     * @model required="true" ordered="false"
     * @generated
     */
    ISOccurence getOccurence();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getOccurence
     * <em>Occurence</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Occurence</em>' reference.
     * @see #getOccurence()
     * @generated
     */
    void setOccurence(ISOccurence value);

    /**
     * Returns the value of the '<em><b>Detectability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Detectability</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Detectability</em>' reference.
     * @see #setDetectability(ISDetectability)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Detectability()
     * @model required="true" ordered="false"
     * @generated
     */
    ISDetectability getDetectability();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getDetectability
     * <em>Detectability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Detectability</em>' reference.
     * @see #getDetectability()
     * @generated
     */
    void setDetectability(ISDetectability value);

    /**
     * Returns the value of the '<em><b>Probability</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Probability</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Probability</em>' reference.
     * @see #setProbability(ISProbability)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_Probability()
     * @model required="true" ordered="false"
     * @generated
     */
    ISProbability getProbability();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getProbability
     * <em>Probability</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Probability</em>' reference.
     * @see #getProbability()
     * @generated
     */
    void setProbability(ISProbability value);

    /**
     * Returns the value of the '<em><b>Feared Event</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Feared Event</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Feared Event</em>' attribute.
     * @see #setFearedEvent(boolean)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_FearedEvent()
     * @model dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
     * @generated
     */
    boolean isFearedEvent();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#isFearedEvent
     * <em>Feared Event</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Feared Event</em>' attribute.
     * @see #isFearedEvent()
     * @generated
     */
    void setFearedEvent(boolean value);

    /**
     * Returns the value of the '<em><b>Failure Mode</b></em>' reference.
     * It is bidirectional and its opposite is
     * '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getFailureEvent <em>Failure Event</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Failure Mode</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Failure Mode</em>' reference.
     * @see #setFailureMode(ISFailureMode)
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISDysfunctionsPackage#getSFailureEvent_FailureMode()
     * @see org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureMode#getFailureEvent
     * @model opposite="failureEvent" ordered="false"
     * @generated
     */
    ISFailureMode getFailureMode();

    /**
     * Sets the value of the '{@link org.polarsys.esf.esfsafetyconcepts.sdysfuctions.ISFailureEvent#getFailureMode
     * <em>Failure Mode</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Failure Mode</em>' reference.
     * @see #getFailureMode()
     * @generated
     */
    void setFailureMode(ISFailureMode value);

} // ISFailureEvent
