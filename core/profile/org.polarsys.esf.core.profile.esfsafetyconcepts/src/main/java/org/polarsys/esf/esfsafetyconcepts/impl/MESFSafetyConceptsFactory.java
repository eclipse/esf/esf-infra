/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *      ALL4TEC & CEA LIST - initial API and implementation
 *******************************************************************************/
package org.polarsys.esf.esfsafetyconcepts.impl;

import org.polarsys.esf.esfsafetyconcepts.IESFSafetyConceptsFactory;
import org.polarsys.esf.esfsafetyconcepts.IMESFSafetyConceptsFactory;
import org.polarsys.esf.esfsafetyconcepts.IMSSafetyArtifacts;

/**
 * This factory overrides the generated factory {@link ESFSafetyConceptsFactory}
 * and returns the new generated interfaces.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 186 $
 */
public class MESFSafetyConceptsFactory
    extends ESFSafetyConceptsFactory
    implements IMESFSafetyConceptsFactory {

    /**
     * Default constructor.
     */
    public MESFSafetyConceptsFactory() {
        super();
    }

    /**
     * @return The initialised factory
     */
    public static IMESFSafetyConceptsFactory init() {
        IESFSafetyConceptsFactory vFactory = ESFSafetyConceptsFactory.init();

        // Check if the factory returned by the standard implementation can suit,
        // otherwise create a new instance
        if (!(vFactory instanceof IMESFSafetyConceptsFactory)) {
            vFactory = new MESFSafetyConceptsFactory();
        }

        return (IMESFSafetyConceptsFactory) vFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMSSafetyArtifacts createSSafetyArtifacts() {
        IMSSafetyArtifacts vSSafetyArtifacts = new MSSafetyArtifacts();
        return vSSafetyArtifacts;
    }
}
