/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfsafetyconcepts.util;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.profile.esfsafetyconcepts.set.ESFSafetyConceptsSet;
import org.polarsys.esf.core.utils.ModelUtil;
import org.polarsys.esf.esfsafetyconcepts.ISSafetyArtifacts;
import org.polarsys.esf.esfsafetyconcepts.impl.SSafetyArtifacts;

/**
 * The utility class for dealing with SafetyArtifacts package.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFSafetyConceptsUtil {
    /** SSafetyArtifacts package name. */
    public static final String SSAFETYARTIFACTS_PACKAGE_NAME = "SafetyArtifacts"; //$NON-NLS-1$

    /** Create SSafetyArtifacts package label. */
    public static final String CREATE_SSAFETYARTIFACTS_LABEL = "Create SSafetyArtifacts"; //$NON-NLS-1$

    /**
     * Default constructor, private as it's a utility class.
     */
    private ESFSafetyConceptsUtil() {
        // Nothing to do
    }

    /**
     * Get SSafetyArtifacts.
     *
     * @param pModelRoot The UML Model root
     * @return The SSafetyArtifacts
     */
    public static ISSafetyArtifacts getSSafetyArtifacts(final Model pModelRoot) {
        ISSafetyArtifacts vSSafetyArtifacts = retrieveSSafetyArtifacts(pModelRoot);

        if (vSSafetyArtifacts == null) {
            createSSafetyArtifacts(pModelRoot);
            vSSafetyArtifacts = retrieveSSafetyArtifacts(pModelRoot);
        }

        return vSSafetyArtifacts;
    }

    /**
     * Retrieve SSafetyArtifacts.
     *
     * @param pModelRoot The UML Model root
     * @return The SSafetyArtifacts
     */
    private static ISSafetyArtifacts retrieveSSafetyArtifacts(final Model pModelRoot) {
        ISSafetyArtifacts vSafetyArtifacts = null;
        Boolean vFound = false;
        List<Package> vPackagesList = pModelRoot.getNestedPackages();
        Iterator<Package> vIterator = vPackagesList.iterator();

        while (!vFound && vIterator.hasNext()) {
            Package vPackage = vIterator.next();
            EObject vStereotypeApplication = UMLUtil.getStereotypeApplication(vPackage, ISSafetyArtifacts.class);
            if (vStereotypeApplication != null) {
                vFound = true;
                vSafetyArtifacts = (ISSafetyArtifacts) vStereotypeApplication;
            }
        }
        return vSafetyArtifacts;
    }

    /**
     *  Create SSafetyArtifacts.
     *
     * @param pModelRoot The UML Model root
     */
    public static void createSSafetyArtifacts(final Model pModelRoot) {
        TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(pModelRoot);

        RecordingCommand vCreateSSafetyArtifacts =
            new RecordingCommand(vDomain, CREATE_SSAFETYARTIFACTS_LABEL) {

                @Override
                protected void doExecute() {
                    // Create the object 'SafetyArtifacts' package
                    Package vSafetyArtifacts = pModelRoot.createNestedPackage(SSAFETYARTIFACTS_PACKAGE_NAME);
                    
                    // Apply 'SSafetyConcepts' profile on 'safetyArtifacts' package
                    Profile vESFSafetyConceptsProfile = (Profile) PackageUtil
            				.loadPackage(URI.createURI(ESFSafetyConceptsSet.PROFILE_PATH), vSafetyArtifacts.eResource().getResourceSet());
                    vSafetyArtifacts.applyProfile(vESFSafetyConceptsProfile);

                    // Apply 'SSafetyArtifacts' stereotype on 'SafetyArtifacts' package
                    StereotypeUtil.apply(vSafetyArtifacts, SSafetyArtifacts.class);
                }
            };

        // Verify if command can be executed
        if (vCreateSSafetyArtifacts.canExecute()) {
            // Execute command
            vDomain.getCommandStack().execute(vCreateSSafetyArtifacts);
        }
    }

}
