/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.profile.esfarchitectureconcepts.tests.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.polarsys.esf.core.profile.esfarchitectureconcepts.set.ESFArchitectureConceptsSet;

/**
 * ESFArchitectureConcepts test util.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFArchitectureConceptsTestUtil {

    /** Unnamed. */
    private static final String UNNAMED = "unnamed"; //$NON-NLS-1$

    /**
     * Default constructor, private as it's a utility class.
     */
    private ESFArchitectureConceptsTestUtil() {
        // Nothing to do
    }

    /**
     * Creates a model with ESFArchitectureConcepts profile applied.
     *
     * @param pResourceSet
     * the resource set in which to create the model
     * @return the created model
     */
    public static Model createESFModel(ResourceSet pResourceSet) {
        return createESFModel(pResourceSet, UNNAMED + "." + UMLResource.FILE_EXTENSION, UNNAMED); //$NON-NLS-1$
    }

    /**
     * Creates a model with ESFArchitectureConcepts profile applied.
     *
     * @param pResourceSet the resource set in which to create the model
     * @param pResourceName name of the resource
     * @param pModelName name of the model
     * @return the created model
     */
    public static Model createESFModel(
        final ResourceSet pResourceSet,
        final String pResourceName,
        final String pModelName) {
        Model vModel = UMLFactory.eINSTANCE.createModel();
        vModel.setName(pModelName);
        Resource vResource = pResourceSet.createResource(URI.createURI(pResourceName));
        vResource.getContents().add(vModel);
        // Retrieve ESFArchitectureConcepts profile and apply with subprofile
        Resource vESFArchConceptsResource =
            pResourceSet.getResource(URI.createURI(ESFArchitectureConceptsSet.PROFILE_PATH), true);
        Profile vESFArchConceptsProfile = (Profile) EcoreUtil
            .getObjectByType(vESFArchConceptsResource.getContents(), UMLPackage.Literals.PACKAGE);
        vModel.applyProfile(vESFArchConceptsProfile);
        return vModel;
    }
}
