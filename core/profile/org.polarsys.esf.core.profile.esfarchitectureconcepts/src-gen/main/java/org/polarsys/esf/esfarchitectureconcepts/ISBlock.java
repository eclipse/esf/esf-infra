/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.common.util.EList;

import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SBlock</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getBase_Class <em>Base Class</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#isTopBlock <em>Top Block</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPartsList <em>Owned SParts List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getUsagesList <em>Usages List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortRolesList <em>Owned SPort Roles List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortsList <em>Owned SPorts List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSModel <em>SModel</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSConnectorsList <em>SConnectors List</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock()
 * @model
 * @generated
 */
public interface ISBlock
		extends IAbstractSArchitectureElement {

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Top Block</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Top Block</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Top Block</em>' attribute.
	 * @see #setTopBlock(boolean)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_TopBlock()
	 * @model default="false" dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isTopBlock();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#isTopBlock <em>Top Block</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Top Block</em>' attribute.
	 * @see #isTopBlock()
	 * @generated
	 */
	void setTopBlock(boolean value);

	/**
	 * Returns the value of the '<em><b>Owned SParts List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISPart}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned SParts List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owned SParts List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_OwnedSPartsList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getOwner
	 * @model opposite="owner" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPart> getOwnedSPartsList();

	/**
	 * Returns the value of the '<em><b>Usages List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISPart}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usages List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Usages List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_UsagesList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getType
	 * @model opposite="type" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPart> getUsagesList();

	/**
	 * Returns the value of the '<em><b>Owned SPort Roles List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned SPort Roles List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owned SPort Roles List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_OwnedSPortRolesList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getOwner
	 * @model opposite="owner" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPortRole> getOwnedSPortRolesList();

	/**
	 * Returns the value of the '<em><b>Owned SPorts List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISPort}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned SPorts List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owned SPorts List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_OwnedSPortsList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort#getOwner
	 * @model opposite="owner" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPort> getOwnedSPortsList();

	/**
	 * Returns the value of the '<em><b>SModel</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISModel#getOwnedSBlocksList <em>Owned SBlocks List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SModel</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SModel</em>' reference.
	 * @see #setSModel(ISModel)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_SModel()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISModel#getOwnedSBlocksList
	 * @model opposite="ownedSBlocksList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISModel getSModel();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getSModel <em>SModel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>SModel</em>' reference.
	 * @see #getSModel()
	 * @generated
	 */
	void setSModel(ISModel value);

	/**
	 * Returns the value of the '<em><b>SConnectors List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISConnector}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SConnectors List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>SConnectors List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSBlock_SConnectorsList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector#getOwner
	 * @model opposite="owner" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISConnector> getSConnectorsList();

} // ISBlock
