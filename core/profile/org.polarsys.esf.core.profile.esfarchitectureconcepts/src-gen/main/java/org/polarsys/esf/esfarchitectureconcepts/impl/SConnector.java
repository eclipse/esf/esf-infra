/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.uml2.uml.Connector;

import org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;

import org.polarsys.esf.esfcore.impl.AbstractSArchitectureElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SConnector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector#getBase_Connector <em>Base Connector</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector#getOwner <em>Owner</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector#getSourcesList <em>Sources List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector#getTargetsList <em>Targets List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.impl.SConnector#getEndsList <em>Ends List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SConnector
		extends AbstractSArchitectureElement
		implements ISConnector {

	/**
	 * The cached value of the '{@link #getBase_Connector() <em>Base Connector</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getBase_Connector()
	 * @generated
	 * @ordered
	 */
	protected Connector base_Connector;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected SConnector() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IESFArchitectureConceptsPackage.Literals.SCONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Connector getBase_Connector() {
		if (base_Connector != null && base_Connector.eIsProxy()) {
			InternalEObject oldBase_Connector = (InternalEObject) base_Connector;
			base_Connector = (Connector) eResolveProxy(oldBase_Connector);
			if (base_Connector != oldBase_Connector) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, IESFArchitectureConceptsPackage.SCONNECTOR__BASE_CONNECTOR, oldBase_Connector, base_Connector));
			}
		}
		return base_Connector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Connector basicGetBase_Connector() {
		return base_Connector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setBase_Connector(Connector newBase_Connector) {
		Connector oldBase_Connector = base_Connector;
		base_Connector = newBase_Connector;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, IESFArchitectureConceptsPackage.SCONNECTOR__BASE_CONNECTOR, oldBase_Connector, base_Connector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ISBlock getOwner() {
		ISBlock owner = basicGetOwner();
		return owner != null && owner.eIsProxy() ? (ISBlock) eResolveProxy((InternalEObject) owner) : owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ISBlock basicGetOwner() {
		// TODO: implement this method to return the 'Owner' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void setOwner(ISBlock newOwner) {
		// TODO: implement this method to set the 'Owner' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSConnectableElement> getSourcesList() {
		// TODO: implement this method to return the 'Sources List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSConnectableElement> getTargetsList() {
		// TODO: implement this method to return the 'Targets List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public EList<IAbstractSConnectableElement> getEndsList() {
		// TODO: implement this method to return the 'Ends List' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SCONNECTOR__BASE_CONNECTOR:
			if (resolve)
				return getBase_Connector();
			return basicGetBase_Connector();
		case IESFArchitectureConceptsPackage.SCONNECTOR__OWNER:
			if (resolve)
				return getOwner();
			return basicGetOwner();
		case IESFArchitectureConceptsPackage.SCONNECTOR__SOURCES_LIST:
			return getSourcesList();
		case IESFArchitectureConceptsPackage.SCONNECTOR__TARGETS_LIST:
			return getTargetsList();
		case IESFArchitectureConceptsPackage.SCONNECTOR__ENDS_LIST:
			return getEndsList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SCONNECTOR__BASE_CONNECTOR:
			setBase_Connector((Connector) newValue);
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__OWNER:
			setOwner((ISBlock) newValue);
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__SOURCES_LIST:
			getSourcesList().clear();
			getSourcesList().addAll((Collection<? extends IAbstractSConnectableElement>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__TARGETS_LIST:
			getTargetsList().clear();
			getTargetsList().addAll((Collection<? extends IAbstractSConnectableElement>) newValue);
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__ENDS_LIST:
			getEndsList().clear();
			getEndsList().addAll((Collection<? extends IAbstractSConnectableElement>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SCONNECTOR__BASE_CONNECTOR:
			setBase_Connector((Connector) null);
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__OWNER:
			setOwner((ISBlock) null);
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__SOURCES_LIST:
			getSourcesList().clear();
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__TARGETS_LIST:
			getTargetsList().clear();
			return;
		case IESFArchitectureConceptsPackage.SCONNECTOR__ENDS_LIST:
			getEndsList().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case IESFArchitectureConceptsPackage.SCONNECTOR__BASE_CONNECTOR:
			return base_Connector != null;
		case IESFArchitectureConceptsPackage.SCONNECTOR__OWNER:
			return basicGetOwner() != null;
		case IESFArchitectureConceptsPackage.SCONNECTOR__SOURCES_LIST:
			return !getSourcesList().isEmpty();
		case IESFArchitectureConceptsPackage.SCONNECTOR__TARGETS_LIST:
			return !getTargetsList().isEmpty();
		case IESFArchitectureConceptsPackage.SCONNECTOR__ENDS_LIST:
			return !getEndsList().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // SConnector
