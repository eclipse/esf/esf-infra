/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;
import org.polarsys.esf.esfarchitectureconcepts.ISModel;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;
import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;
import org.polarsys.esf.esfcore.IAbstractSElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage
 * @generated
 */
public class ESFArchitectureConceptsAdapterFactory
		extends AdapterFactoryImpl {

	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static IESFArchitectureConceptsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ESFArchitectureConceptsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = IESFArchitectureConceptsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance
	 * object of the model.
	 * <!-- end-user-doc -->
	 * 
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ESFArchitectureConceptsSwitch<Adapter> modelSwitch = new ESFArchitectureConceptsSwitch<Adapter>() {
		@Override
		public Adapter caseSPort(ISPort object) {
			return createSPortAdapter();
		}

		@Override
		public Adapter caseAbstractSConnectableElement(IAbstractSConnectableElement object) {
			return createAbstractSConnectableElementAdapter();
		}

		@Override
		public Adapter caseSConnector(ISConnector object) {
			return createSConnectorAdapter();
		}

		@Override
		public Adapter caseSBlock(ISBlock object) {
			return createSBlockAdapter();
		}

		@Override
		public Adapter caseSPart(ISPart object) {
			return createSPartAdapter();
		}

		@Override
		public Adapter caseSPortRole(ISPortRole object) {
			return createSPortRoleAdapter();
		}

		@Override
		public Adapter caseSModel(ISModel object) {
			return createSModelAdapter();
		}

		@Override
		public Adapter caseAbstractSElement(IAbstractSElement object) {
			return createAbstractSElementAdapter();
		}

		@Override
		public Adapter caseAbstractSArchitectureElement(IAbstractSArchitectureElement object) {
			return createAbstractSArchitectureElementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param target
	 *            the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort <em>SPort</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort
	 * @generated
	 */
	public Adapter createSPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement <em>Abstract SConnectable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement
	 * @generated
	 */
	public Adapter createAbstractSConnectableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.ISConnector <em>SConnector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISConnector
	 * @generated
	 */
	public Adapter createSConnectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock <em>SBlock</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock
	 * @generated
	 */
	public Adapter createSBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart <em>SPart</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart
	 * @generated
	 */
	public Adapter createSPartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole <em>SPort Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole
	 * @generated
	 */
	public Adapter createSPortRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfarchitectureconcepts.ISModel <em>SModel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISModel
	 * @generated
	 */
	public Adapter createSModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSElement <em>Abstract SElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfcore.IAbstractSElement
	 * @generated
	 */
	public Adapter createAbstractSElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.polarsys.esf.esfcore.IAbstractSArchitectureElement <em>Abstract SArchitecture Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.polarsys.esf.esfcore.IAbstractSArchitectureElement
	 * @generated
	 */
	public Adapter createAbstractSArchitectureElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // ESFArchitectureConceptsAdapterFactory
