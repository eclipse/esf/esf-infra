/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.polarsys.esf.esfarchitectureconcepts.IAbstractSConnectableElement;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;
import org.polarsys.esf.esfarchitectureconcepts.ISModel;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;
import org.polarsys.esf.esfcore.IAbstractSArchitectureElement;
import org.polarsys.esf.esfcore.IAbstractSElement;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage
 * @generated
 */
public class ESFArchitectureConceptsSwitch<T>
		extends Switch<T> {

	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static IESFArchitectureConceptsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ESFArchitectureConceptsSwitch() {
		if (modelPackage == null) {
			modelPackage = IESFArchitectureConceptsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param ePackage
	 *            the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case IESFArchitectureConceptsPackage.SPORT: {
			ISPort sPort = (ISPort) theEObject;
			T result = caseSPort(sPort);
			if (result == null)
				result = caseAbstractSConnectableElement(sPort);
			if (result == null)
				result = caseAbstractSArchitectureElement(sPort);
			if (result == null)
				result = caseAbstractSElement(sPort);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFArchitectureConceptsPackage.ABSTRACT_SCONNECTABLE_ELEMENT: {
			IAbstractSConnectableElement abstractSConnectableElement = (IAbstractSConnectableElement) theEObject;
			T result = caseAbstractSConnectableElement(abstractSConnectableElement);
			if (result == null)
				result = caseAbstractSArchitectureElement(abstractSConnectableElement);
			if (result == null)
				result = caseAbstractSElement(abstractSConnectableElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFArchitectureConceptsPackage.SCONNECTOR: {
			ISConnector sConnector = (ISConnector) theEObject;
			T result = caseSConnector(sConnector);
			if (result == null)
				result = caseAbstractSArchitectureElement(sConnector);
			if (result == null)
				result = caseAbstractSElement(sConnector);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFArchitectureConceptsPackage.SBLOCK: {
			ISBlock sBlock = (ISBlock) theEObject;
			T result = caseSBlock(sBlock);
			if (result == null)
				result = caseAbstractSArchitectureElement(sBlock);
			if (result == null)
				result = caseAbstractSElement(sBlock);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFArchitectureConceptsPackage.SPART: {
			ISPart sPart = (ISPart) theEObject;
			T result = caseSPart(sPart);
			if (result == null)
				result = caseAbstractSArchitectureElement(sPart);
			if (result == null)
				result = caseAbstractSElement(sPart);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFArchitectureConceptsPackage.SPORT_ROLE: {
			ISPortRole sPortRole = (ISPortRole) theEObject;
			T result = caseSPortRole(sPortRole);
			if (result == null)
				result = caseAbstractSConnectableElement(sPortRole);
			if (result == null)
				result = caseAbstractSArchitectureElement(sPortRole);
			if (result == null)
				result = caseAbstractSElement(sPortRole);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case IESFArchitectureConceptsPackage.SMODEL: {
			ISModel sModel = (ISModel) theEObject;
			T result = caseSModel(sModel);
			if (result == null)
				result = caseAbstractSArchitectureElement(sModel);
			if (result == null)
				result = caseAbstractSElement(sModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPort</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPort</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPort(ISPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SConnectable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SConnectable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSConnectableElement(IAbstractSConnectableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SConnector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SConnector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSConnector(ISConnector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SBlock</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SBlock</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSBlock(ISBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPart</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPart</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPart(ISPart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPort Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPort Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPortRole(ISPortRole object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SModel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SModel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSModel(ISModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSElement(IAbstractSElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract SArchitecture Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract SArchitecture Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractSArchitectureElement(IAbstractSArchitectureElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * 
	 * @param object
	 *            the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} // ESFArchitectureConceptsSwitch
