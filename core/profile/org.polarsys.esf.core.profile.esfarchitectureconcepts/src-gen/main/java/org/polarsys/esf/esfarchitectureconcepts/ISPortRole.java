/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.uml2.uml.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPort Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getBase_Port <em>Base Port</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getType <em>Type</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getOwner <em>Owner</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getUsageContext <em>Usage Context</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPortRole()
 * @model
 * @generated
 */
public interface ISPortRole
		extends IAbstractSConnectableElement {

	/**
	 * Returns the value of the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Port</em>' reference.
	 * @see #setBase_Port(Port)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPortRole_Base_Port()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Port getBase_Port();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getBase_Port <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Port</em>' reference.
	 * @see #getBase_Port()
	 * @generated
	 */
	void setBase_Port(Port value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getRolesList <em>Roles List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ISPort)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPortRole_Type()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPort#getRolesList
	 * @model opposite="rolesList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISPort getType();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ISPort value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortRolesList <em>Owned SPort Roles List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(ISBlock)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPortRole_Owner()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortRolesList
	 * @model opposite="ownedSPortRolesList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlock getOwner();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(ISBlock value);

	/**
	 * Returns the value of the '<em><b>Usage Context</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPart#getSPortRolesList <em>SPort Roles List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Usage Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Usage Context</em>' reference.
	 * @see #setUsageContext(ISPart)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPortRole_UsageContext()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPart#getSPortRolesList
	 * @model opposite="sPortRolesList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISPart getUsageContext();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getUsageContext <em>Usage Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Usage Context</em>' reference.
	 * @see #getUsageContext()
	 * @generated
	 */
	void setUsageContext(ISPart value);

} // ISPortRole
