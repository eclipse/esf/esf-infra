/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.uml2.uml.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPort</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getRolesList <em>Roles List</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getOwner <em>Owner</em>}</li>
 * <li>{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getBase_Port <em>Base Port</em>}</li>
 * </ul>
 *
 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPort()
 * @model
 * @generated
 */
public interface ISPort
		extends IAbstractSConnectableElement {

	/**
	 * Returns the value of the '<em><b>Roles List</b></em>' reference list.
	 * The list contents are of type {@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole}.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles List</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Roles List</em>' reference list.
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPort_RolesList()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISPortRole#getType
	 * @model opposite="type" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ISPortRole> getRolesList();

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortsList <em>Owned SPorts List</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(ISBlock)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPort_Owner()
	 * @see org.polarsys.esf.esfarchitectureconcepts.ISBlock#getOwnedSPortsList
	 * @model opposite="ownedSPortsList" required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ISBlock getOwner();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(ISBlock value);

	/**
	 * Returns the value of the '<em><b>Base Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Base Port</em>' reference.
	 * @see #setBase_Port(Port)
	 * @see org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsPackage#getSPort_Base_Port()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Port getBase_Port();

	/**
	 * Sets the value of the '{@link org.polarsys.esf.esfarchitectureconcepts.ISPort#getBase_Port <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Base Port</em>' reference.
	 * @see #getBase_Port()
	 * @generated
	 */
	void setBase_Port(Port value);

} // ISPort
