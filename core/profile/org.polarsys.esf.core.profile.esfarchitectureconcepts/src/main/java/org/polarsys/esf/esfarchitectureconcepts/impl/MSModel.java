/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfarchitectureconcepts.IMSModel;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;

/**
 * This class can override the generated class {@link SModel}
 * and will be used by the custom factory.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSModel
    extends SModel
    implements IMSModel {

    /**
     * Default constructor.
     */
    public MSModel() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return GenericAbstractSElement.getName(getBase_Package());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUUID() {
        return GenericAbstractSElement.getUUID(getBase_Package());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISBlock> getOwnedSBlocksList() {
        EList<ISBlock> vSBlocksList = new BasicEList<ISBlock>();
        Package vPackage = getBase_Package();

        if (vPackage != null) {
            /*
             * Look for all elements stereotyped SBlock and get them
             */
            for (Element vElement : vPackage.getOwnedElements()) {
                if ((vElement instanceof Class) && (UMLUtil.getStereotypeApplication(vElement, SBlock.class) != null)) {
                    vSBlocksList.add((SBlock) UMLUtil.getStereotypeApplication(vElement, SBlock.class));
                }
            }
        }

        UnmodifiableEList<ISBlock> vUSBlocksList = new UnmodifiableEList<ISBlock>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getSModel_OwnedSBlocksList(),
            vSBlocksList.size(),
            vSBlocksList.toArray());
        return vUSBlocksList;
    }
}
