/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfarchitectureconcepts.IMSBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISConnector;
import org.polarsys.esf.esfarchitectureconcepts.ISModel;
import org.polarsys.esf.esfarchitectureconcepts.ISPart;
import org.polarsys.esf.esfarchitectureconcepts.ISPort;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;

/**
 * This class can override the generated class {@link SBlock}
 * and will be used by the custom factory.
 *
 * @author $Author: jdumont $
 * @version $Revision: 176 $
 */
public class MSBlock
    extends SBlock
    implements IMSBlock {

    /**
     * Default constructor.
     */
    public MSBlock() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return GenericAbstractSElement.getName(getBase_Class());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUUID() {
        return GenericAbstractSElement.getUUID(getBase_Class());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SModel basicGetSModel() {
        SModel vSModel = null;
        Class vBase = getBase_Class();
        if (vBase != null) {
            Model vModel = vBase.getModel();
            // Get the SModel if the stereotype is applied
            if (UMLUtil.getStereotypeApplication(vModel, SModel.class) != null) {
                vSModel = UMLUtil.getStereotypeApplication(vModel, SModel.class);
            }
        }
        return vSModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISPart> getUsagesList() {
        EList<ISPart> vSPartsList = new BasicEList<ISPart>();
        ISModel vSModel = getSModel();
        if (vSModel != null) {
            Package vPackage = vSModel.getBase_Package();
            if (vPackage != null) {
                // Walk the model and look for all sPart of type sBlock
                for (Element vEl : vPackage.allOwnedElements()) {
                    if (UMLUtil.getStereotypeApplication(vEl, SPart.class) != null) {
                        ISPart vSPart = (SPart) UMLUtil.getStereotypeApplication(vEl, SPart.class);
                        if (vSPart.getType().equals(this)) {
                            vSPartsList.add(vSPart);
                        }
                    }
                }
            }
        }

        UnmodifiableEList<ISPart> vUSPartsList = new UnmodifiableEList<ISPart>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getSBlock_UsagesList(),
            vSPartsList.size(),
            vSPartsList.toArray());
        return vUSPartsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISPart> getOwnedSPartsList() {
        EList<ISPart> vSPartsList = new BasicEList<ISPart>();
        Class vBase = getBase_Class();
        if (vBase != null) {
            for (Property vProperty : vBase.getOwnedAttributes()) {
                if (UMLUtil.getStereotypeApplication(vProperty, SPart.class) != null) {
                    ISPart vSPart = (SPart) UMLUtil.getStereotypeApplication(vProperty, SPart.class);
                    vSPartsList.add(vSPart);
                }
            }
        }

        UnmodifiableEList<ISPart> vUSPartsList = new UnmodifiableEList<ISPart>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getSBlock_OwnedSPartsList(),
            vSPartsList.size(),
            vSPartsList.toArray());
        return vUSPartsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISConnector> getSConnectorsList() {
        EList<ISConnector> vSConnectorsList = new BasicEList<ISConnector>();
        Class vBase = getBase_Class();
        if (vBase != null) {
            for (Connector vConnector : vBase.getOwnedConnectors()) {
                if (UMLUtil.getStereotypeApplication(vConnector, SConnector.class) != null) {
                    ISConnector vSConnector =
                        (SConnector) UMLUtil.getStereotypeApplication(vConnector, SConnector.class);
                    vSConnectorsList.add(vSConnector);
                }
            }
        }

        UnmodifiableEList<ISConnector> vUSConnectorsList = new UnmodifiableEList<ISConnector>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getSBlock_SConnectorsList(),
            vSConnectorsList.size(),
            vSConnectorsList.toArray());
        return vUSConnectorsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISPort> getOwnedSPortsList() {
        EList<ISPort> vSPortsList = new BasicEList<ISPort>();
        Class vBase = getBase_Class();
        if (vBase != null) {
            for (Port vPort : vBase.getOwnedPorts()) {
                if (UMLUtil.getStereotypeApplication(vPort, SPort.class) != null) {
                    ISPort vSPort =
                        (SPort) UMLUtil.getStereotypeApplication(vPort, SPort.class);
                    vSPortsList.add(vSPort);
                }
            }
        }

        UnmodifiableEList<ISPort> vUSPortsList = new UnmodifiableEList<ISPort>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getSBlock_OwnedSPortsList(),
            vSPortsList.size(),
            vSPortsList.toArray());
        return vUSPortsList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISPortRole> getOwnedSPortRolesList() {
        EList<ISPortRole> vSPortRolesList = new BasicEList<ISPortRole>();

        // TODO: Retrieve all owned SPortRoles of SBlock

        UnmodifiableEList<ISPortRole> vUSPortRolesList = new UnmodifiableEList<ISPortRole>(
            this,
            ESFArchitectureConceptsPackage.eINSTANCE.getSBlock_OwnedSPortRolesList(),
            vSPortRolesList.size(),
            vSPortRolesList.toArray());
        return vUSPortRolesList;
    }
}
