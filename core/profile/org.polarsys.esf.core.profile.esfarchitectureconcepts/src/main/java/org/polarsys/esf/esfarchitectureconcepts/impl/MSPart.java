/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.esfarchitectureconcepts.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.esfarchitectureconcepts.IESFArchitectureConceptsFactory;
import org.polarsys.esf.esfarchitectureconcepts.IMSPart;
import org.polarsys.esf.esfarchitectureconcepts.ISBlock;
import org.polarsys.esf.esfarchitectureconcepts.ISPortRole;
import org.polarsys.esf.esfcore.impl.GenericAbstractSElement;

/**
 * This class can override the generated class {@link SPart}
 * and will be used by the custom factory.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class MSPart
    extends SPart
    implements IMSPart {

    /**
     * Default constructor.
     */
    public MSPart() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return GenericAbstractSElement.getName(getBase_Property());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUUID() {
        return GenericAbstractSElement.getUUID(getBase_Property());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ISBlock basicGetType() {
        Property vBase = getBase_Property();
        Class vType = null;
        ISBlock vSBlock = IESFArchitectureConceptsFactory.eINSTANCE.createSBlock();

        if (vBase != null) {
            // Retrieve the type of base element (Class)
            if ((vBase.getType() != null) && (vBase.getType() instanceof Class)) {
                vType = (Class) vBase.getType();
            }

            // Retrieve the stereotype application (SBlock) of the type
            if ((vType != null) && (UMLUtil.getStereotypeApplication(vType, SBlock.class) != null)) {
                vSBlock = (SBlock) UMLUtil.getStereotypeApplication(vType, SBlock.class);
            }
        }
        return vSBlock;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ISBlock basicGetOwner() {
        Property vBase = getBase_Property();
        ISBlock vSBlock = IESFArchitectureConceptsFactory.eINSTANCE.createSBlock();

        if (vBase != null) {
            // Check if the SPart's owner is a SBlock
            if ((vBase.getOwner() instanceof Class)
                && (UMLUtil.getStereotypeApplication(vBase.getOwner(), SBlock.class) != null)) {
                vSBlock = (SBlock) UMLUtil.getStereotypeApplication(vBase.getOwner(), SBlock.class);
            }
        }
        return vSBlock;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EList<ISPortRole> getSPortRolesList() {
        EList<ISPortRole> vSPortRolesList = new BasicEList<ISPortRole>();

        // TODO: Retrieve all SPortRoles of SPart

        UnmodifiableEList<ISPortRole> vUSPortRolesList =
            new UnmodifiableEList<ISPortRole>(
                this,
                ESFArchitectureConceptsPackage.eINSTANCE.getSPart_SPortRolesList(),
                vSPortRolesList.size(),
                vSPortRolesList.toArray());
        return vUSPortRolesList;
    }
}
