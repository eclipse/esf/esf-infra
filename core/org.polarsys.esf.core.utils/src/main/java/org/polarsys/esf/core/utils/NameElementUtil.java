/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.utils;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;

/**
 * Utilities for the manager of the element's name.
 *
 * @author $Author: jdumont $
 * @version $Revision: 168 $
 */
public final class NameElementUtil {

    /** Command label. */
    private static final String COMMAND_LABEL = "NameElementHelper: Create Library"; //$NON-NLS-1$

    /**
     * Default constructor, private as it's a utility class.
     */
    private NameElementUtil() {
        // Nothing to do
    }

    /**
     * Nominate manager into owner.
     *
     * @param pOwner Owner concerned
     * @param pNewElement New element to name
     */
    public static void nameManagerIntoOwner(final Element pOwner, final NamedElement pNewElement) {
        // Get the element valid name within the owner context
        String vValidName = getValidNameIntoOwner(pOwner, pNewElement);

        if (StringUtils.isNotEmpty(vValidName)) {
            // If valid name does not match with the current name, the element must be renamed
            if (!vValidName.equals(pNewElement.getName())) {
                renameElement(vValidName, pNewElement);
            }
        }
    }

    /**
     * Get a valid name into owner from a base name.
     *
     * @param pOwner Owner concerned
     * @param pBaseName Base name used to build the valid name
     * @return Valid name found
     */
    public static String getValidNameIntoOwner(final Element pOwner, final String pBaseName) {
        return findValidNameIntoOwner(pOwner, pBaseName);
    }

    /**
     * Valid name into owner based by type's name or element's name.
     *
     * @param pOwner Owner concerned
     * @param pNewElement Element for which a name is searched
     * @return Valid name found
     */
    public static String getValidNameIntoOwner(final Element pOwner, final NamedElement pNewElement) {
        String vBaseName = StringUtils.EMPTY;

        if (pNewElement instanceof Property) {
            if (((Property) pNewElement).getType() != null) {
                vBaseName = ((Property) pNewElement).getType().getName();
            } else {
                vBaseName = pNewElement.getName();
            }

            vBaseName = vBaseName.toLowerCase(Locale.ENGLISH);

        } else {
            vBaseName = pNewElement.getName();
        }

        return findValidNameIntoOwner(pOwner, vBaseName);
    }

    /**
     * Find a valid name into the owner context, from the given base name.
     *
     * @param pOwner Owner concerned
     * @param pBaseName Base used to build the valid name
     * @return Valid name found
     */
    private static String findValidNameIntoOwner(final Element pOwner, final String pBaseName) {
        int i = 0;
        boolean vNotValidName = true;
        String vValidName = pBaseName;

        while (vNotValidName) {
            vNotValidName = false;
            for (Element vElement : pOwner.getOwnedElements()) {
                if (vElement instanceof NamedElement) {
                    NamedElement vNamedElement = (NamedElement) vElement;
                    if (vNamedElement.getName().matches(vValidName)) {
                        vNotValidName = true;
                        i++;
                        break;
                    }
                }
            }

            // If the base name is not valid
            if (i > 0) {
                vValidName = pBaseName + i;
            }
        }

        return vValidName;
    }

    /**
     * Rename element of the model.
     *
     * @param pNewName New name value
     * @param pElement Element to rename
     */
    public static void renameElement(final String pNewName, final NamedElement pElement) {

        TransactionalEditingDomain vEditingDomain = ModelUtil.getTransactionalEditingDomain(pElement);
        RecordingCommand vRenameCommand = new RecordingCommand(vEditingDomain, COMMAND_LABEL) {

            @Override
            protected void doExecute() {
                pElement.setName(pNewName);
            }
        };

        if (vRenameCommand.canExecute()) {
            vEditingDomain.getCommandStack().execute(vRenameCommand);
        }
    }
}
