/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.utils;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.papyrus.infra.viewpoints.policy.PolicyChecker;
import org.eclipse.papyrus.infra.viewpoints.policy.ViewPrototype;
import org.eclipse.uml2.uml.Element;

/**
 * Utility class for ESF Tables.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ESFTablesUtil {

    /**
     * Default constructor, private as it's a utility class.
     */
    private ESFTablesUtil() {
        // Nothing to do
    }

    /**
     * Get The ViewPrototype of the Table by its ID.
     *
     * @param pTableID The Table ID
     * @return The Table ViewPrototype
     */
    public static ViewPrototype getViewPrototype(final String pTableID, final Element pRootElement) {
        Collection<ViewPrototype> vPrototypes = PolicyChecker.getFor(pRootElement).getAllPrototypes();
        ViewPrototype vTable = null;
        String vPotentialTableID = null;
        Iterator<ViewPrototype> vIterator = vPrototypes.iterator();
        while (vTable == null && vIterator.hasNext()) {
            ViewPrototype vPotentialPrototype = vIterator.next();
            vPotentialTableID = vPotentialPrototype.getRepresentationKind().getImplementationID();
            if (pTableID.equals(vPotentialTableID)) {
                vTable = vPotentialPrototype;
            }
        }

        return vTable;
    }

    /**
     * Create a table.
     *
     * @param pTableID The Table ID
     * @param pTableName The Table Name
     * @param pRootElement The element where should be created the table.
     */
    public static void createTable(final String pTableID, final String pTableName, final Element pRootElement) {
        ViewPrototype vTable = getViewPrototype(pTableID, pRootElement);
        if (vTable != null) {
            vTable.instantiateOn(pRootElement, pTableName + vTable.getViewCountOn(pRootElement));
        }
    }
}
