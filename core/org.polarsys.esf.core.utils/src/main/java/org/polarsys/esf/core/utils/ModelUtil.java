/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForEObject;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.uml.tools.model.UmlModel;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;

/**
 * Utilities for loading and the manipulation of models.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public final class ModelUtil {

    /** Label of the command to create a new element. */
    private static final String COMMAND_LABEL_CREATE_NEW_ELEMENT = "ModelUtil: Create new element"; //$NON-NLS-1$

    /** Message of the error during the service registry search. */
    private static final String ERROR_SERVICE_REAGITRY_SEARCH =
        "Error during the service registry search"; //$NON-NLS-1$

    /**
     * Default constructor, private as it's a utility class.
     */
    private ModelUtil() {
        // Nothing to do
    }

    /**
     * Test if the selected element is one of the good type and <b>return
     * element if good, null otherwise</b>.
     *
     * @param pSelection Object to test
     * @param pRefClass Object wanted for the selection
     * @return An EObject if good, <code>null</code> otherwise
     */
    public static EObject getSelectedEObjectOfType(final ISelection pSelection, final EClass pRefClass) {
        EObject vSelectedEObjectOfType = null;

        // Check if input selection is conform
        if (pSelection instanceof IStructuredSelection) {

            // Get the first element of the mouse selected element (on screen)
            StructuredSelection vTreeSelection = (StructuredSelection) pSelection;
            Object vSelectedObject = vTreeSelection.getFirstElement();

            // When we get the first element we verify that the EObject is not null
            if (EMFHelper.getEObject(vSelectedObject) == null) {
                vSelectedEObjectOfType = null;
            }

            vSelectedEObjectOfType = EMFHelper.getEObject(vSelectedObject);

            // test if selected element is correct type (or correct subtype)
            EClass vEObjectClass = vSelectedEObjectOfType.eClass();

            if (!pRefClass.isSuperTypeOf(vEObjectClass)) {
                vSelectedEObjectOfType = null;
            }
        }

        return vSelectedEObjectOfType;
    }

    /**
     * Get top element of the given element.
     *
     * @param pElement Starting point of the search
     * @return Top element found
     */
    private static Element getTopElement(final Element pElement) {
        Element vTopElement = null;

        if (pElement.getOwner() != null) {
            vTopElement = getTopElement(pElement.getOwner());
        } else {
            vTopElement = pElement;
        }

        return vTopElement;
    }

    /**
     * Get transactional editing domain.
     *
     * @param pElement Element for which an editing domain is searched
     * @return TransactionalEditingDomain found
     */
    public static TransactionalEditingDomain getTransactionalEditingDomain(final Element pElement) {
        TransactionalEditingDomain vEditingDomain = null;
        Model vModel = null;

        if (pElement instanceof Model) {
            vModel = (Model) pElement;
        } else {
            vModel = ModelUtil.getTopModel(pElement);
        }

        if (vModel != null) {
            ServicesRegistry vServiceRegistry = null;
            try {
                vServiceRegistry = ServiceUtilsForEObject.getInstance().getServiceRegistry(vModel);
            } catch (final ServiceException pException) {
                CoreUtilsActivator.logError(ERROR_SERVICE_REAGITRY_SEARCH, pException);
            }

            try {
                vEditingDomain = vServiceRegistry.getService(TransactionalEditingDomain.class);
            } catch (final ServiceException pException) {
                CoreUtilsActivator.logError("Error during the editing domain search", pException); //$NON-NLS-1$
            }
        }

        return vEditingDomain;
    }

    /**
     * Get all elements typed by Class from an element.
     *
     * @param pElement Given element
     * @return List of elements typed by Class
     */
    public static List<Element> getElementsTypedByClass(final Element pElement) {
        List<Element> vElementsList = Collections.emptyList();

        // Verify if element is a type (Class)
        if (pElement instanceof Class) {
            vElementsList = pElement.allOwnedElements();

        } else if (pElement instanceof Property) {
            // Or if it is a property of type Class
            if (((Property) pElement).getType() instanceof Class) {
                // Get its element type and then get its owned elements
                Class vElementType = (Class) ((Property) pElement).getType();
                vElementsList = vElementType.allOwnedElements();
            }
        }

        return vElementsList;
    }

    /**
     * Get the top model from a given element.
     *
     * @param pElement Starting point of the search
     * @return Model found
     */
    public static Model getTopModel(final Element pElement) {
        Model vTopModel = null;

        // Get the top element from the given one
        Element vTopElement = getTopElement(pElement);

        // Ensure that the top element is a model
        if (vTopElement instanceof Model) {
            vTopModel = (Model) vTopElement;
        }

        return vTopModel;
    }

    /**
     * Create a new base Class element.
     *
     * @param pOwner Element owner of the new base Class element
     * @param pName Name of the new base Class element
     * @return The new base Class element
     */
    public static Class createNewBaseClassElement(final Package pOwner, final String pName) {
        Class vNewClass = null;
        TransactionalEditingDomain vEditingDomain = ModelUtil.getTransactionalEditingDomain(pOwner);
        RecordingCommand vCreateNewElementCmd = new RecordingCommand(vEditingDomain, COMMAND_LABEL_CREATE_NEW_ELEMENT) {

            /** New Class created by the command. */
            private Class mNewClass = null;

            /**
             * {@inheritDoc}
             */
            @Override
            protected void doExecute() {
                // Create new Class
                mNewClass = pOwner.createOwnedClass(pName, true);
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public Collection<?> getResult() {
                Collection<?> vResult = Collections.EMPTY_LIST;
                // Retrieve the new Class created by this command
                if (mNewClass != null) {
                    vResult = Collections.singletonList(mNewClass);
                }

                return vResult;
            }
        };

        // Verify if command can be executed
        if (vCreateNewElementCmd.canExecute()) {
            // Execute command
            vEditingDomain.getCommandStack().execute(vCreateNewElementCmd);

            // Retrieve the new Class created by execution
            Iterator<?> vIterator = vCreateNewElementCmd.getResult().iterator();
            while (vNewClass == null && vIterator.hasNext()) {
                Object vObject = vIterator.next();
                if (vObject instanceof Class) {
                    vNewClass = (Class) vObject;
                }
            }
        }

        // Return the new Class created
        return vNewClass;
    }

    /**
     * Get the working model.
     *
     * @return The model that is modified inside Papyrus
     */
    public static Model getWorkingModel() {
        Model vWorkingModel = null;

        ModelSet vModelSet = new ModelSet();
        vModelSet = getCurrentModelSet();
        UmlModel vIUMLModel = (UmlModel) vModelSet.getModel(UmlModel.MODEL_ID);
        try {
            vWorkingModel = (Model) vIUMLModel.lookupRoot();
        } catch (final NotFoundException pException) {
            CoreUtilsActivator.logError("The root of model is not found", pException); //$NON-NLS-1$
        }
        return vWorkingModel;
    }

    /**
     * Get the current model set.
     *
     * @return The current model set of Papyrus.
     */
    public static ModelSet getCurrentModelSet() {
        ModelSet vModelSet = new ModelSet();

        IWorkbenchPart vPart = getCurrentWorkbenchPart();
        if (vPart instanceof IMultiDiagramEditor) {
            ServicesRegistry vRegistry = ((IMultiDiagramEditor) vPart).getServicesRegistry();
            try {
                vModelSet = vRegistry.getService(ModelSet.class);
            } catch (final ServiceException pException) {
                CoreUtilsActivator.logError(ERROR_SERVICE_REAGITRY_SEARCH, pException);
            }
        }
        return vModelSet;
    }

    /**
     * Used to get the the current workbenchPart.
     *
     * @return The current workbench part
     */
    public static IWorkbenchPart getCurrentWorkbenchPart() {
        IWorkbenchPart vPart = null;
        try {
            IWorkbenchWindow vActiveWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
            if (vActiveWorkbenchWindow != null) {
                IWorkbenchPage vActivePage = vActiveWorkbenchWindow.getActivePage();
                if (vActivePage != null) {
                    if (vActivePage.getActiveEditor() instanceof IMultiDiagramEditor) {
                        vPart = vActivePage.getActiveEditor();
                    }
                }
            }
        } catch (final NullPointerException pException) {
            CoreUtilsActivator.logError("Error during the active workbench window search", pException); //$NON-NLS-1$
        }

        return vPart;
    }
}
