/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.editingsupport;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertyEditingSupport;
import org.polarsys.esf.core.common.ui.constants.ImageConstants;

/**
 * This class is a custom {@link PropertyEditingSupport} that provides a validation.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ValidatedPropertyEditingSupport
    extends PropertyEditingSupport {

    /** The cell editor used by this instance. */
    private CellEditor mCellEditor = null;
    
    /** The status line manager used to display the validation messages. */
    private IStatusLineManager mStatusLineManager = null;

    /**
     * The default constructor.
     * 
     * @param pViewer The viewer which will allow to display, edit and validate the properties
     * @param pPropertySourceProvider The property source provider used
     * @param pPropertyId The id of the property edited
     * @param pStatusLineManager The status line manager used to display the validation messages
     */
    public ValidatedPropertyEditingSupport(
        final ColumnViewer pViewer, 
        final IPropertySourceProvider pPropertySourceProvider, 
        final Object pPropertyId,
        final IStatusLineManager pStatusLineManager) {

        // Call the parent method
        super(pViewer, pPropertySourceProvider, pPropertyId);
        
        mStatusLineManager = pStatusLineManager;
    }

    /**
     * {@inheritDoc}
     * 
     * This is overridden, to manage the cell validation and display
     * the potential messages in the status bar.
     */
    @Override
    protected CellEditor getCellEditor(final Object pObject) {
        // Try to get the cell editor corresponding to the given objet
        // by calling the parent method
        // NB : If the cell is in read only mode, this will return null
        mCellEditor = super.getCellEditor(pObject);

        if (mCellEditor != null) {

            // If the cell editor is not null, add a new listener on it
            // to add the validation mechanisms on it
            mCellEditor.addListener(new ICellEditorListener() {

                /**
                 * {@inheritDoc}
                 * 
                 * This implementation aim is to display error message when the value is not valid.
                 */
                @Override
                public void editorValueChanged(final boolean pOldValidState, final boolean pNewValidState) {
                    if (!pNewValidState) {
                        // The new state is not valid, thus display an error message
                        setStatusLineMessage(mCellEditor.getErrorMessage(), true);
                    } else {
                        // The new state is valid, clear the status message
                        setStatusLineMessage(null, false);
                    }
                }

                /**
                 * {@inheritDoc}
                 */
                @Override
                public void cancelEditor() {
                    // Simply clear the message on a cancel
                    setStatusLineMessage(null, false);
                }

                /**
                 * {@inheritDoc}
                 */
                @Override
                public void applyEditorValue() {
                    // Nothing to do
                }
            });
        }
        
        return mCellEditor;
    }

    /**
     * {@inheritDoc}
     * 
     * If there is no error the value is saved and the status line message is cleared.
     * Otherwise, display an error message.
     * 
     * @param pCellEditor the cell-editor
     * @param pViewerCell the cell the editor is working for
     */
    @Override
    protected void saveCellEditorValue(final CellEditor pCellEditor, final ViewerCell pViewerCell) {
        
        // Check if the cell editor consider the value as valid
        if (pCellEditor.isValueValid()) {
            // Clear the status message
            setStatusLineMessage(null, false);
            
            // Call the parent method to save the value in the model
            super.saveCellEditorValue(pCellEditor, pViewerCell);    
            
        } else {
            // The value is not valid, display an error message and don't save the new value
            setStatusLineMessage(pCellEditor.getErrorMessage(), true);
        }
    }
    
    /**
     * Set the message to the status line manager.
     * It can be treated as an error message or not according to the flag given in parameter.
     * 
     * @param pMessage The message to set
     * @param pIsErrorMsg <code>true</code> if the message is an error message, <code>false</code> otherwise
     */
    private void setStatusLineMessage(final String pMessage, final boolean pIsErrorMsg) {

        if (mStatusLineManager != null) {
            // Update the status message, with an error status or not
            if (pIsErrorMsg) {
                mStatusLineManager.setErrorMessage(ImageConstants.ICON_ERROR, pMessage);
            } else {
                mStatusLineManager.setMessage(ImageConstants.ICON_INFO, pMessage);
            }
        }
    }
}
