/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.widget;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TreeColumn;


/**
 * Utility class used to centralise common methods to work with tree viewers. 
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class TreeViewerUtils {
    
    
    /**
     * Default constructor.
     * Private as its a utility class.
     */
    private TreeViewerUtils() {
    }
    
    /**
     * Create a {@link TreeViewerColumn} and set its alignment, text and size.
     * 
     * @param pTreeViewer The tree viewer which will contain the column
     * @param pText The column text
     * @param pAlignment The column content alignment
     * @param pWidth The column width
     * @return The created column
     */
    public static TreeViewerColumn createViewerColumn(
        final TreeViewer pTreeViewer,
        final String pText,
        final int pAlignment,
        final int pWidth) {

        // Create the tree viewer column
        final TreeViewerColumn vTreeViewerColumn = new TreeViewerColumn(pTreeViewer, SWT.NONE);

        // Get the column object and set its alignment, text and width
        final TreeColumn vTreeColumn = vTreeViewerColumn.getColumn();
        vTreeColumn.setText(pText);
        vTreeColumn.setAlignment(pAlignment);
        vTreeColumn.setWidth(pWidth);

        return vTreeViewerColumn;
    }
    
    /**
     * Create a {@link TreeViewerColumn} with a specific label provider,
     * and set its alignment, text and size.
     * 
     * @param pTreeViewer The tree viewer which will contain the column
     * @param pText The column text
     * @param pAlignment The column content alignment
     * @param pWidth The column width
     * @param pLabelProvider The custom label provider to set to this column. Can be null.
     * @return The created column
     */
    public static TreeViewerColumn createViewerColumn(
        final TreeViewer pTreeViewer,
        final String pText,
        final int pAlignment,
        final int pWidth,
        final CellLabelProvider pLabelProvider) {

        // Create the tree viewer column
        final TreeViewerColumn vTreeViewerColumn = createViewerColumn(pTreeViewer, pText, pAlignment, pWidth);
        
        // Finally, if a label provider is specified, use it
        if (pLabelProvider != null) {
            vTreeViewerColumn.setLabelProvider(pLabelProvider);
        }

        return vTreeViewerColumn;
    }
    
    /**
     * Create a {@link TreeViewerColumn} with specific label provider and editing support,
     * and set its alignment, text and size.
     * 
     * @param pTreeViewer The tree viewer which will contain the column
     * @param pAlignment The column content alignment
     * @param pText The column text
     * @param pWidth The column width
     * @param pLabelProvider The custom label provider to set to this column. Can be null.
     * @param pEditingSupport The custom editing support to set to this column. Can be null.
     * @return The created column
     */
    public static TreeViewerColumn createViewerColumn(
        final TreeViewer pTreeViewer,
        final String pText,
        final int pAlignment,
        final int pWidth,
        final CellLabelProvider pLabelProvider,
        final EditingSupport pEditingSupport) {

        // Create the tree viewer column
        final TreeViewerColumn vTreeViewerColumn = 
            createViewerColumn(pTreeViewer, pText, pAlignment, pWidth, pLabelProvider);
        
        // Finally, if an editing support is specified, use it
        if (pEditingSupport != null) {
            vTreeViewerColumn.setEditingSupport(pEditingSupport);
        }

        return vTreeViewerColumn;
    }

}
