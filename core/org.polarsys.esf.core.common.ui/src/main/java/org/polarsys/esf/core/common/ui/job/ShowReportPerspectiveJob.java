/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.job;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.polarsys.esf.core.common.ui.CommonUIActivator;

/**
 * Job used to offer to the user to open Report perspective.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 90 $
 * 
 */
public class ShowReportPerspectiveJob
    implements Runnable {

    /** Message of dialog box. */
    private static final String PERSPECTIVE_DIALOG_MSG =
        CommonUIActivator.getMessages().getString("ShowReportPerspectiveJob.msg.dialogbox"); //$NON-NLS-1$

    /** Title of dialog box. */
    private static final String PERSPECTIVE_DIALOG_TITLE =
        CommonUIActivator.getMessages().getString("ShowReportPerspectiveJob.title.dialogbox"); //$NON-NLS-1$

    /** Id of reports perspective. */
    private static final String ID_PERSPECTIVE_REPORTS = "org.polarsys.esf.core.ui.perspective.reports"; //$NON-NLS-1$

    /**
     * Default constructor.
     */
    public ShowReportPerspectiveJob() {
    }

    @Override
    public void run() {
        // Get current window
        IWorkbenchWindow vCurrentWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();

        // Check if reports perspective is not the current one
        if (!ID_PERSPECTIVE_REPORTS.equals(vCurrentWindow.getActivePage().getPerspective().getId())) {

            // Ask to the user if he wants to change its perspective
            boolean vReturnCode = MessageDialog.openQuestion(
                vCurrentWindow.getShell(),
                PERSPECTIVE_DIALOG_TITLE,
                PERSPECTIVE_DIALOG_MSG);

            if (vReturnCode) {
                try {
                    // Open the reports perspective
                    PlatformUI.getWorkbench().showPerspective(
                        ID_PERSPECTIVE_REPORTS,
                        vCurrentWindow);

                } catch (final WorkbenchException pException) {
                    CommonUIActivator.logError(pException.getMessage(), pException);
                }
            }

        }
    }
}
