/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui;

import java.util.ResourceBundle;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.jface.resource.ImageRegistry;
import org.polarsys.esf.core.common.messages.Messages;

/**
 * Central singleton for the common ui tools part of ESF.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class CommonUIActivator
    extends EMFPlugin {

    /** Keep track of the encapsulating singleton. */
    public static final CommonUIActivator INSTANCE = new CommonUIActivator();

    /** Keep track of the implementation singleton. */
    private static Implementation sPlugin = null;

    /** Messages class used to find localised string. */
    private static Messages sMessages = new Messages(ResourceBundle.getBundle(Messages.BUNDLE_NAME));

    /**
     * The default constructor.
     */
    public CommonUIActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * Returns the singleton instance of the Eclipse plugin, used as resource locator.
     *
     * @return The singleton instance
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     *
     * @return The singleton instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * @return The messages class used to return localised string
     */
    public static Messages getMessages() {
        return sMessages;
    }

    /**
     * Create an Error status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logError(final String pMessage, final Exception pException) {
        // Create the Error status
        IStatus vStatus = new Status(IStatus.ERROR, sPlugin.getSymbolicName(), IStatus.ERROR, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create a Warning status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logWarning(final String pMessage, final Exception pException) {
        // Create the Warning status
        IStatus vStatus = new Status(IStatus.WARNING, sPlugin.getSymbolicName(), IStatus.WARNING, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create an Info status with the data given in parameter and log it.
     *
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logInfo(final String pMessage, final Exception pException) {
        // Create the Info status
        IStatus vStatus = new Status(IStatus.INFO, sPlugin.getSymbolicName(), IStatus.INFO, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     *
     * Unique implementation of common ui activator.
     *
     *
     * @author $Author: jdumont $
     * @version $Revision: 83 $
     */
    public static class Implementation
        extends EclipseUIPlugin {

        /** Key for icon up image. */
        public static final String ICON_UP_IMG_KEY = "icon.up"; //$NON-NLS-1$

        /** Key for icon down image. */
        public static final String ICON_DOWN_IMG_KEY = "icon.down"; //$NON-NLS-1$

        /** Key for icon add image. */
        public static final String ICON_ADD_IMG_KEY = "icon.add"; //$NON-NLS-1$

        /** Key for icon remove image. */
        public static final String ICON_REMOVE_IMG_KEY = "icon.remove"; //$NON-NLS-1$

        /** Key for Show advanced fields button. */
        public static final String ICON_ADVANCED_SHOW_KEY = "icon.showadvanced"; //$NON-NLS-1$

        /** Key for Hide advanced fields button. */
        public static final String ICON_ADVANCED_HIDE_KEY = "icon.hideadvanced"; //$NON-NLS-1$

        /** Key for expand all image. */
        public static final String ICON_EXPAND_ALL_KEY = "icon.expandall"; //$NON-NLS-1$

        /** Key for collapse all image. */
        public static final String ICON_COLLAPSE_ALL_KEY = "icon.collapseall"; //$NON-NLS-1$

        /** Key for the search icon. */
        public static final String ICON_SEARCH_KEY = "icon.search"; //$NON-NLS-1$

        /** Key for the banner in the probability edition dialog. */
        public static final String ICON_PROBABILITY_DIALOG_KEY = "banner.probability.dialog"; //$NON-NLS-1$

        /** Key for Weibull probability rule. */
        public static final String ICON_WEIBULL = "rule.weibull"; //$NON-NLS-1$

        /** Key for Poisson probability rule. */
        public static final String ICON_POISSON = "rule.poisson"; //$NON-NLS-1$

        /** Key for exponential probability rule. */
        public static final String ICON_EXPONENTIAL = "rule.exponential"; //$NON-NLS-1$

        /** Icons folder. */
        private static final String ICONS_FOLDER = "src/main/resources/icons/"; //$NON-NLS-1$

        /**
         * Creates an instance.
         */
        public Implementation() {
            super();

            // Remember the static instance.
            sPlugin = this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void initializeImageRegistry(final ImageRegistry pReg) {
            super.initializeImageRegistry(pReg);

            // Icon paths are not externalised as they are no reason to modify it or to access to it externally
            String vSymbolicName = getPlugin().getSymbolicName();

            pReg.put(
                ICON_UP_IMG_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "icon_up.png")); //$NON-NLS-1$
            pReg.put(
                ICON_DOWN_IMG_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "icon_down.png")); //$NON-NLS-1$
            pReg.put(
                ICON_ADD_IMG_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "icon_addone.png")); //$NON-NLS-1$
            pReg.put(
                ICON_REMOVE_IMG_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "icon_removeone.png")); //$NON-NLS-1$
            pReg.put(
                ICON_ADVANCED_SHOW_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "advanced_show.png")); //$NON-NLS-1$
            pReg.put(
                ICON_ADVANCED_HIDE_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "advanced_hide.png")); //$NON-NLS-1$
            pReg.put(
                ICON_EXPAND_ALL_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "expand_all.gif")); //$NON-NLS-1$
            pReg.put(
                ICON_COLLAPSE_ALL_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "collapse_all.gif")); //$NON-NLS-1$
            pReg.put(
                ICON_SEARCH_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "search.gif")); //$NON-NLS-1$

            pReg.put(
                ICON_PROBABILITY_DIALOG_KEY,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "dialog_probability.png")); //$NON-NLS-1$
            pReg.put(
                ICON_POISSON,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "rule/icon_poisson_rule.png")); //$NON-NLS-1$
            pReg.put(
                ICON_WEIBULL,
                imageDescriptorFromPlugin(vSymbolicName, ICONS_FOLDER + "rule/icon_weibull_rule.png")); //$NON-NLS-1$
            pReg.put(
                ICON_EXPONENTIAL,
                imageDescriptorFromPlugin(
                    vSymbolicName,
                    ICONS_FOLDER + "rule/icon_exponential_rule.png")); //$NON-NLS-1$
        }
    }

}
