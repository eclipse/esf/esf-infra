/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.filter;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.uml2.uml.NamedElement;

/**
 * Custom implementation of filter, using the name property
 * of the object on which is applied the filter.
 *
 * The object must implements {@link IAbstractNamedObject}, otherwise
 * the parent {@link ToStringFilter} is applied.
 *
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class NamePropertyFilter
    extends ToStringFilter {

    /**
     * Default constructor.
     */
    public NamePropertyFilter() {
        super();
    }

    /**
     * {@inheritDoc}
     *
     * Return the result of the {@link #toString()} method applied
     * on the element, to apply the filter on this value.
     *
     * @return The value of the {@link #toString()} method applied on the given element
     */
    @Override
    protected String getFilterPropertyValue(final Object pElement) {
        String vFilteredValue = StringUtils.EMPTY;

        if (pElement instanceof NamedElement) {
            // Get the name value to apply the filter
            vFilteredValue = ((NamedElement) pElement).getName();
        } else {
            // Call the parent method to get the toString value to apply the filter
            vFilteredValue = super.getFilterPropertyValue(pElement);
        }

        return vFilteredValue;
    }
}
