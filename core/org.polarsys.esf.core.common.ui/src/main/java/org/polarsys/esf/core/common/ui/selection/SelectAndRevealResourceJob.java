/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.selection;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ISetSelectionTarget;
import org.eclipse.ui.progress.UIJob;
import org.polarsys.esf.core.common.ui.CommonUIActivator;

/**
 * Job to select a resource in project explorer and reveal it.
 * The resource content is also refreshed.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 * 
 */
public class SelectAndRevealResourceJob
    extends UIJob {

    /** Title for the job. */
    private static final String JOB_TITLE =
        CommonUIActivator.getMessages().getString("SelectAndRevealResourceJob.title"); //$NON-NLS-1$

    /** Resource to select and reveal. */
    private IResource mResource = null;

    /**
     * Default constructor.
     * 
     * @param pResource Resource to select and reveal
     */
    public SelectAndRevealResourceJob(final IResource pResource) {
        super(JOB_TITLE);
        mResource = pResource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IStatus runInUIThread(final IProgressMonitor pMonitor) {
        if (mResource != null) {
            try {
                // Refresh the resource content
                mResource.refreshLocal(IResource.DEPTH_INFINITE, pMonitor);
            } catch (final CoreException pException) {
                CommonUIActivator.logError("Resource refresh failed", pException); //$NON-NLS-1$
            }

            // Get Project Explorer view
            IWorkbench vWorkbench = PlatformUI.getWorkbench();
            IWorkbenchPage vActivePage = vWorkbench.getActiveWorkbenchWindow().getActivePage();
            IWorkbenchPart vActivePart = vActivePage.findView(IPageLayout.ID_PROJECT_EXPLORER);

            // Select and reveal the new resource in Project Explorer
            if (vActivePart instanceof ISetSelectionTarget) {
                ISelection vTargetSelection = new StructuredSelection(mResource);
                ((ISetSelectionTarget) vActivePart).selectReveal(vTargetSelection);
            }
        }
        return Status.OK_STATUS;
    }

}
