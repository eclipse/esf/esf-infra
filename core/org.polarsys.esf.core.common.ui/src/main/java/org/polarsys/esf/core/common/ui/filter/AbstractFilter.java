/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.filter;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 * Abstract filter used as parent for any viewer filter implementation,
 * based on a regular expression pattern.
 *
 * The different implementations must implement the {@link AbstractFilter#getFilterPropertyValue(Object)}
 * method, which is used to define on which property the filter must be applied.
 * For example, the ToStringFilter will simply base the filter on the result of the toString
 * method applied on the object.
 *
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public abstract class AbstractFilter
    extends ViewerFilter {

    /** The 'slash' character. */
    private static final String SLASH_CHAR = "\\"; //$NON-NLS-1$

    /** The 'slash' regular expression. */
    private static final String SLASH_CHAR_REGEX = "\\\\"; //$NON-NLS-1$

    /** The 'dot' character. */
    private static final String DOT_CHAR = "."; //$NON-NLS-1$

    /** The 'dot' regular expression. */
    private static final String DOT_CHAR_REGEX = "\\."; //$NON-NLS-1$

    /** The 'any string' character. */
    private static final String ANY_STRING = "*"; //$NON-NLS-1$

    /** The 'any string' regular expression. */
    private static final String ANY_STRING_REGEX = ".*"; //$NON-NLS-1$

    /** The 'any' character. */
    private static final String ANY_CHARACTER = "?"; //$NON-NLS-1$

    /** The 'any character' regular expression. */
    private static final String ANY_CHARACTER_REGEX = ".?"; //$NON-NLS-1$

    /** The pattern used to do the filter. */
    private Pattern mPattern = null;

    /**
     * Default constructor.
     */
    public AbstractFilter() {
        super();
    }

    /**
     * Setter for the filter. It takes a String in parameter
     * and used it to build the filter pattern, using a regular expression.
     *
     * @param pFilterText The text used to build the filter pattern
     */
    public void setFilter(final String pFilterText) {
        if (StringUtils.isEmpty(pFilterText)) {
            // Reset the pattern is the text is empty
            mPattern = null;

        } else {
            // Format the given filter text to build a pattern expression
            String vFormatedFilterText = pFilterText;
            vFormatedFilterText = vFormatedFilterText.replace(SLASH_CHAR, SLASH_CHAR_REGEX);
            vFormatedFilterText = vFormatedFilterText.replace(DOT_CHAR, DOT_CHAR_REGEX);
            vFormatedFilterText = vFormatedFilterText.replace(ANY_STRING, ANY_STRING_REGEX);
            vFormatedFilterText = vFormatedFilterText.replace(ANY_CHARACTER, ANY_CHARACTER_REGEX);
            vFormatedFilterText = ANY_STRING_REGEX + vFormatedFilterText + ANY_STRING_REGEX;

            // Finally build the corresponding pattern
            mPattern = Pattern.compile(vFormatedFilterText);
        }
    }

    /**
     * {@inheritDoc}
     *
     * Return <code>true</code> if an element must be displayed. This means that the node or at
     * least one of its children matches the filter.
     *
     * @param pViewer The viewer containing all the elements
     * @param pParentElement The parent of the element to check
     * @param pElement The element to check
     * @return <code>true</code> if the element must be displayed
     */
    @Override
    public boolean select(final Viewer pViewer, final Object pParentElement, final Object pElement) {

        boolean vSelectElement = true;

        // Ensure that the current pattern value is valid
        if (mPattern != null) {

            // Check if the current element matches the filter
            vSelectElement = isMatchesWithFilter(pElement);

            // If the current element is not selected by the filter,
            // ensure that a valid viewer is known to check if one of
            // its children is selected
            if (!vSelectElement && pViewer instanceof TreeViewer) {
                // Cast the viewer
                final TreeViewer vTreeViewer = (TreeViewer) pViewer;

                // Get the content provider of the viewer, and extract the children
                // of the current element from it
                final ITreeContentProvider vTreeContentProvider =
                    (ITreeContentProvider) vTreeViewer.getContentProvider();
                final Object[] vChildrenArray = vTreeContentProvider.getChildren(pElement);

                if (vChildrenArray.length != 0) {
                    // If there is any children, check if one of them is selected by the filter
                    vSelectElement = isAnyChildrenMatchesWithFilter(pViewer, pElement, vChildrenArray);
                }
            }
        }
        return vSelectElement;
    }

    /**
     * Return <code>true</code> if any children matches with the filter.
     *
     * @param pViewer The viewer containing all the elements
     * @param pElement The element to check
     * @param pChildrensArray The array of children to check
     * @return <code>true</code> if any children matches with the filter
     */
    private boolean isAnyChildrenMatchesWithFilter(
        final Viewer pViewer,
        final Object pElement,
        final Object[] pChildrensArray) {

        boolean vFound = false;

        // Loop on the children to check if any matches with the filter,
        // or if any of its owned children matches
        for (int i = 0; i < pChildrensArray.length && !vFound; i++) {
            vFound = isMatchesWithFilter(pChildrensArray[i]) || select(pViewer, pElement, pChildrensArray[i]);
        }

        return vFound;
    }

    /**
     * Return <code>true</code> if the element given in parameter
     * matches with the filter.
     *
     * @param pElement The element to check
     * @return <code>true</code> if the element matches with the filter
     */
    private boolean isMatchesWithFilter(final Object pElement) {
        // Get the filter value
        String vFilterValue = getFilterPropertyValue(pElement);

        // Remove all the controls characters like the carriage return
        String vFormatedFilterValue = vFilterValue;
        // TODO : Reimplement StringFormater.removeControlChars(vFilterValue);

        // Try to apply the filter pattern, to know if the value is matching
        return mPattern.matcher(vFormatedFilterValue).matches();
    }

    /**
     * Return the property value on which the filter must be applied.
     *
     * @param pElement The element used as source to get the property value
     * @return Return the property value on which the filter must be applied
     */
    protected abstract String getFilterPropertyValue(final Object pElement);
}
