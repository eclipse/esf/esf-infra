/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.view.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.emf.common.ui.celleditor.ExtendedComboBoxCellEditor;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.widgets.Composite;

/**
 * This class is a {@link PropertyDescriptor} that manage the Java Generic.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ExtendedPropertyDescriptor
    extends PropertyDescriptor {

    /**
     * Default constructor.
     * 
     * @param pObject The object
     * @param pItemPropertyDescriptor The property descriptor {@link IItemPropertyDescriptor}
     */
    public ExtendedPropertyDescriptor(final Object pObject, final IItemPropertyDescriptor pItemPropertyDescriptor) {
        super(pObject, pItemPropertyDescriptor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellEditor createPropertyEditor(final Composite pComposite) {

        // Initialise with
        CellEditor vCellEditor = null;

        if (itemPropertyDescriptor.canSetProperty(object)) {

            // Specific case of EStructuralFeature
            if (object instanceof EObject) {

                // Get feature of object
                final EObject vObject = (EObject) object;
                final Object vGenericFeature = itemPropertyDescriptor.getFeature(object);

                if (vGenericFeature instanceof EStructuralFeature) {

                    if (getPropertyValue(itemPropertyDescriptor, vObject) instanceof Boolean) {

                        vCellEditor = handleEStructuralFeature(pComposite);
                    }
                }

            }

            // Default
            if (vCellEditor == null) {
                // Get cell editor from ancestor
                vCellEditor = super.createPropertyEditor(pComposite);
            }
        }

        return vCellEditor;
    }

    /**
     * Handle boolean EStructuralFeature dialog property view.
     * 
     * @param pComposite Parent composite
     * @return Cell editor with java generic type
     */
    private CellEditor handleEStructuralFeature(final Composite pComposite) {

        // Create dialog with Java generic values for boolean type
        CellEditor vCellEditor = new ExtendedComboBoxCellEditor(pComposite, Arrays.asList(new Object[] {
            Boolean.FALSE,
            Boolean.TRUE}), getEditLabelProvider(), itemPropertyDescriptor.isSortChoices(object)) {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void refreshItems(final String pFilter) {
                CCombo vCombo = (CCombo) getControl();
                if (vCombo != null && !vCombo.isDisposed()) {
                    List<Object> vOriginalList = new ArrayList<Object>(originalList);
                    String[] vItemsArray = createItems(vOriginalList, labelProvider, pFilter, sorted);
                    if (!vOriginalList.isEmpty() && !vOriginalList.equals(list)) {
                        Object vPreviousValue = getValue();
                        list = vOriginalList;
                        vCombo.setItems(vItemsArray);
                        if (list.contains(vPreviousValue)) {
                            setValue(vPreviousValue);
                        } else if (!list.isEmpty()) {
                            setValue(list.get(0));
                        }
                    }
                }
            }
        };
        return vCellEditor;
    }

    /**
     * Retrieve the property value thanks to the given {@link IItemPropertyDescriptor}.
     * 
     * @param pPropertyDescriptor The given {@link IItemPropertyDescriptor}.
     * @param pObject The value to retrieve
     * @return A property value
     */
    protected Object getPropertyValue(final IItemPropertyDescriptor pPropertyDescriptor, final Object pObject) {

        Object vPropertyValue = null;

        // Verify if descriptor is null
        if (pPropertyDescriptor != null) {
            // Get property value for descriptor
            vPropertyValue = pPropertyDescriptor.getPropertyValue(pObject);

            // If value is ItemPropertySource
            if (vPropertyValue instanceof IItemPropertySource) {
                // Get editable value
                vPropertyValue = ((IItemPropertySource) vPropertyValue).getEditableValue(pObject);
            }

        }
        return vPropertyValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CellEditor createEDataTypeCellEditor(final EDataType pEDataType, final Composite pComposite) {

        CellEditor vCellEditor = null;

        // Verify if it is EAttribute
        if (!itemPropertyDescriptor.isMultiLine(object)) {

            final Object vGenericFeature = itemPropertyDescriptor.getFeature(object);

            if (vGenericFeature instanceof EAttribute) {
                EAttribute vEAttribute = (EAttribute) vGenericFeature;
                if (vEAttribute.getEType() instanceof EDataType) {
                    EDataType vEAttributeDataType = (EDataType) vEAttribute.getEType();

                    if (vEAttributeDataType.equals(EcorePackage.eINSTANCE.getEJavaObject())) {
                        // If EDataType is an instance of EJavaObject
                        // handle java generic type
                        vCellEditor = handleGenericTypeValue(pComposite);

                    }
                }
            }
        }

        if (vCellEditor == null) {
            // Get editor from ancestor
            vCellEditor = super.createEDataTypeCellEditor(pEDataType, pComposite);
        }

        return vCellEditor;
    }

    /**
     * Handle Java generic type to instantiate correct cell editor.
     * 
     * @param pComposite Parent composite
     * @return Cell editor according to Java generic type
     */
    private CellEditor handleGenericTypeValue(final Composite pComposite) {
        EObject vEObject = (EObject) object;
        Object vValue = getPropertyValue(itemPropertyDescriptor, vEObject);

        // Initialise with current value of cell editor
        CellEditor vCellEditor = null;
        EDataType vEDataType = null;

        // Verify generic type to determine data type
        if (vValue instanceof Integer) {
            vEDataType = EcorePackage.eINSTANCE.getEIntegerObject();
        } else if (vValue instanceof Double) {
            vEDataType = EcorePackage.eINSTANCE.getEDoubleObject();
        } else if (vValue instanceof String) {
            vEDataType = EcorePackage.eINSTANCE.getEString();
        }

        // Create with determined data type
        if (vEDataType != null) {
            vCellEditor = new ExtendedEDataTypeCellEditor(
                vEDataType,
                pComposite);
        }

        return vCellEditor;
    }

    /**
     * This class is an {@link EDataTypeCellEditor} that manage the java Generic.
     */
    protected static class ExtendedEDataTypeCellEditor
        extends EDataTypeCellEditor {

        /**
         * Default constructor.
         * 
         * @param pEDataType The {@link EDataType}
         * @param pParent The parent composite
         */
        public ExtendedEDataTypeCellEditor(final EDataType pEDataType, final Composite pParent) {
            super(pEDataType, pParent);
            valueHandler = new EDataTypeValueHandler(pEDataType) {

                /**
                 * {@inheritDoc}
                 */
                @Override
                public String toString(final Object pValue) {
                    String vStringValue = StringUtils.EMPTY;

                    if (pValue != null) {
                        vStringValue = super.toString(pValue);
                    }

                    return vStringValue;

                }
            };
            setValidator(valueHandler);
        }
    }
}
