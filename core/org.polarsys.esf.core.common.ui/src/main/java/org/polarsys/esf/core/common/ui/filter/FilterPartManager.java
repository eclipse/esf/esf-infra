/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.polarsys.esf.core.common.ui.adapter.PartListenerAdapter;

/**
 * Manager for the filter part mechanism.
 * 
 * This is a singleton, which will manage the list of available and active
 * filter listeners.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public final class FilterPartManager {

    /** Keep track of this singleton instance. */
    public static final FilterPartManager INSTANCE = new FilterPartManager();

    /** The default filter text. */
    public static final String DEFAULT_FILTER_TEXT = StringUtils.EMPTY;

    /** The list of all available parts that implement the interface {@link IFilterPartListener}. */
    private List<IFilterPartListener> mAvailablePartListenersList = new ArrayList<IFilterPartListener>();

    /** The list of all the parts which are currently listening the change of the text filter. */
    private List<IFilterPartListener> mActivePartListenersList = new ArrayList<IFilterPartListener>();

    /** The current text used to filter. */
    private String mFilterText = DEFAULT_FILTER_TEXT;

    /**
     * Part listener used to react to the close and the opening of any
     * part, to remember of all the available part listeners.
     */
    private final IPartListener mPartListener = new PartListenerAdapter() {

        /**
         * {@inheritDoc}
         */
        @Override
        public void partClosed(final IWorkbenchPart pPart) {
            if (pPart instanceof IFilterPartListener) {
                // Remove the part from the current filter listener
                mActivePartListenersList.remove(pPart);

                // Remove the part from the available filter listener
                FilterPartManager.this.removeAvailableFilterPartListener((IFilterPartListener) pPart);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void partOpened(final IWorkbenchPart pPart) {
            if (pPart instanceof IFilterPartListener) {
                // Add the part to the available filter listener
                FilterPartManager.this.addAvailableFilterPartListener((IFilterPartListener) pPart);
            }
        }

    };

    /**
     * Private constructor, as it's a singleton.
     */
    private FilterPartManager() {
        // Register the part listener to the active workbench
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().addPartListener(mPartListener);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void finalize() throws Throwable {
        // Unregister the part listener from the active workbench
        if (mPartListener != null) {
            IWorkbenchWindow vWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
                
            if (vWorkbenchWindow != null) {
                vWorkbenchWindow.getPartService().removePartListener(mPartListener);
            }
        }
        
        // The call the parent method
        super.finalize();
    }

    /**
     * Add a listener as an available filter part listener.
     * Has no effect if an identical listener is already registered.
     * 
     * @param pFiltrePartListener The filter part listener to add
     */
    private void addAvailableFilterPartListener(final IFilterPartListener pFiltrePartListener) {
        mAvailablePartListenersList.add(pFiltrePartListener);
    }

    /**
     * Remove the given filter part listener from the available listeners.
     * Has no affect if an identical listener is not registered.
     * 
     * @param pFiltrePartListener The filter part listener to remove
     */
    private void removeAvailableFilterPartListener(final IFilterPartListener pFiltrePartListener) {
        mAvailablePartListenersList.remove(pFiltrePartListener);
    }

    /**
     * Add a listener for filter text changes, and apply directly
     * the filter is any value is currently specified.
     * 
     * Has no effect if an identical listener is already registered.
     * 
     * @param pFiltrePartListener The filter part listener to register
     */
    public void addFilterPartListener(final IFilterPartListener pFiltrePartListener) {
        // Add the given part in the active listeners list
        mActivePartListenersList.add(pFiltrePartListener);

        // Remove the given part from the available listeners list,
        // as it is now an active listener
        mAvailablePartListenersList.remove(pFiltrePartListener);

        // If there is currently any filter value, notify the listener to apply the filter directly
        if (!DEFAULT_FILTER_TEXT.equals(mFilterText)) {
            pFiltrePartListener.filterTextChanged(mFilterText);
        }
    }

    /**
     * Remove the given listener from the active listeners and
     * remember of it by adding it to the available listeners list.
     * 
     * Has no affect if an identical listener is not registered.
     * 
     * @param pFiltrePartListener The filter part listener to remove
     */
    public void removeFilterPartListener(final IFilterPartListener pFiltrePartListener) {
        // Remove the listener from the active ones
        mActivePartListenersList.remove(pFiltrePartListener);

        // Remember of the listener by adding it to the available listeners list
        mAvailablePartListenersList.add(pFiltrePartListener);

        // If there is currently any filter value, notify the listener
        // to reset its filter
        if (!DEFAULT_FILTER_TEXT.equals(mFilterText)) {
            // Notify the listener to reset its filter with the default value
            pFiltrePartListener.filterTextChanged(DEFAULT_FILTER_TEXT);
        }
    }

    /**
     * @return The list of available part listeners
     */
    public List<IFilterPartListener> getAvailablePartListenersList() {
        return mAvailablePartListenersList;
    }

    /**
     * @return The list of active part listeners
     */
    public List<IFilterPartListener> getActivePartListenersList() {
        return mActivePartListenersList;
    }

    /**
     * Notify all the active listeners that the filter text has change
     * to allow them to apply the new filter value.
     */
    private void fireFilterTextChanged() {
        for (final IFilterPartListener vListener : mActivePartListenersList) {
            vListener.filterTextChanged(mFilterText);
        }
    }

    /**
     * Update the filter text value and notify all the listeners
     * that the filter has changed.
     * 
     * @param pFilterText The new filter text to apply
     */
    public void updateFilterText(final String pFilterText) {
        // Update the filter value
        mFilterText = pFilterText;

        // Notify all the listeners
        fireFilterTextChanged();
    }

    /**
     * Reset the filter text value and notify all the listeners
     * that the filter has been reseted.
     */
    public void resetFilterText() {
        // Update the filter value with the default text
        updateFilterText(DEFAULT_FILTER_TEXT);
    }
}
