/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.adapter;

import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;


/**
 * A default implementation of the <code>IPartListener</code> interface.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class PartListenerAdapter implements IPartListener {
    
    /**
     * Default constructor.
     */
    public PartListenerAdapter() {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void partActivated(final IWorkbenchPart pPart) {
         // Not implemented
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void partBroughtToTop(final IWorkbenchPart pPart) {
        // Not implemented
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void partClosed(final IWorkbenchPart pPart) {
        // Not implemented
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void partDeactivated(final IWorkbenchPart pPart) {
        // Not implemented
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void partOpened(final IWorkbenchPart pPart) {
        // Not implemented
    }
}
