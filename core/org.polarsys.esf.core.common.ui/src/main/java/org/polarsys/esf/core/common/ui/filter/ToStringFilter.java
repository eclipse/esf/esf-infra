/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.ui.filter;

/**
 * Basic implementation of filter, using the toString method
 * to get the object property on which is applied the filter.
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ToStringFilter
    extends AbstractFilter {

    /**
     * Default constructor.
     */
    public ToStringFilter() {
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * Return the result of the {@link #toString()} method applied
     * on the element, to apply the filter on this value.
     * 
     * @return The value of the {@link #toString()} method applied on the given element
     */
    @Override
    protected String getFilterPropertyValue(final Object pElement) {
        return pElement.toString();
    }
}
