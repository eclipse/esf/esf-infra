/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.metamodel.loader;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

/**
 * 
 * Interface for loader of metamodel instance file.
 * 
 * @param <T> Type of Root element
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IResourceLoader<T extends EObject> {

    /**
     * Load metamodel resource file from URI.
     * 
     * @param pURI EMF URI of file from which model must be created.
     * @return a model if load is success, false otherwise.
     */
    T loadFromURI(final URI pURI);

    /**
     * Method used to load metamodel instance from file.
     * 
     * @param pRelativeFilePath relative path for file where model will be read
     * @return a model
     */
    T load(final String pRelativeFilePath);
}
