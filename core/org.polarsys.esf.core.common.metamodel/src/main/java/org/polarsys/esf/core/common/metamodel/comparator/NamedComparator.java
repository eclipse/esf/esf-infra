/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.metamodel.comparator;

import java.util.Comparator;

import org.eclipse.uml2.uml.NamedElement;

/**
 * Comparator to sort {@link NamedElement} object list.
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class NamedComparator
    implements Comparator<NamedElement> {

    /**
     * Default constructor.
     */
    public NamedComparator() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(final NamedElement pNamed1, final NamedElement pNamed2) {
        // Compare on name
        return pNamed1.getName().compareTo(pNamed2.getName());
    }

}
