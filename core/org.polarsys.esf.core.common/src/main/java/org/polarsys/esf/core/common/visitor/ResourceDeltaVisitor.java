/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.visitor;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;


/**
 * Visitor implementation used to browse the content of the 
 * workspace and find if any resource, managed in a given resource set,
 * have been deleted, or have changed. 
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ResourceDeltaVisitor 
    implements IResourceDeltaVisitor {

    /** The resource set containing all the resources to control during the visit action. */
    private ResourceSet mResourceSet = null;

    /** Collection of resources which have changed since last check. */
    private Collection<Resource> mChangedResourcesCollection = new ArrayList<Resource>();

    /** Collection of resources which have been removed since last check. */
    private Collection<Resource> mRemovedResourcesCollection = new ArrayList<Resource>();

    /**
     * Default constructor.
     * 
     * @param pResourceSet The resource set containing all the resources to control on each visit. 
     *                     Only the workspace file corresponding to these resources will be treated, to find
     *                     which resources have changed, or have been removed 
     */
    public ResourceDeltaVisitor(final ResourceSet pResourceSet) {
        // Remember of the given resource set
        mResourceSet = pResourceSet;
    }
    
    /**
     * {@inheritDoc}
     * 
     * This visitor will find all the resources which have changed or have been deleted.
     */
    @Override
    public boolean visit(final IResourceDelta pDelta) {
        boolean vVisitChildren = false;
        
        // Check the type of the current resource, as it will treat only the files elements
        if (pDelta.getResource().getType() == IResource.FILE) {
            // Check what is the delta kind : only the file removed, or changed
            // are treated. A change result of a save action, which update the content
            // of the file corresponding to the resource. The changes linked to a marker
            // update are not treated
            if (pDelta.getKind() == IResourceDelta.REMOVED 
                || pDelta.getKind() == IResourceDelta.CHANGED
                && pDelta.getFlags() != IResourceDelta.MARKERS) {
                
                // Try to find the resource corresponding to the modified file
                Resource vResource = mResourceSet.getResource(
                    URI.createPlatformResourceURI(pDelta.getFullPath().toString(), true),
                    false);
                
                if (vResource != null) {
                    // The file corresponds to an edited resource, thus according to the modification
                    // type, update the collection of managed resources 
                    if (pDelta.getKind() == IResourceDelta.REMOVED) {
                        // The resource has been removed, remember it 
                        mRemovedResourcesCollection.add(vResource);
                    } else if (pDelta.getKind() == IResourceDelta.CHANGED) {
                        // The resource has changed, remember it 
                        mChangedResourcesCollection.add(vResource);
                    }
                }
            }
        } else {
            // The current resource is not a file, try to visit its children
            vVisitChildren = true;
        }

        return vVisitChildren;
    }

    /**
     * Return the collection of changed resources found. 
     * A change result of a save action, which update the content
     * of the file corresponding to the resource.
     * 
     * @return The collection of changed resources
     */
    public Collection<Resource> getChangedResources() {
        return mChangedResourcesCollection;
    }

    /**
     * @return The collection of removed resources
     */
    public Collection<Resource> getRemovedResources() {
        return mRemovedResourcesCollection;
    }
}
