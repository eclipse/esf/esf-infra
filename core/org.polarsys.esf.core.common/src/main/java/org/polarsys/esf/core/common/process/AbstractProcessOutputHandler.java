/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.common.process;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import org.eclipse.core.runtime.IStatus;

/**
 * Class used to handle output stream for external process (error and normal stream).
 * Offer generic solutions for stream.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public abstract class AbstractProcessOutputHandler implements IProcessOutputHandler {

    /** Standard stream. */
    protected OutputStream mStandardStream = null;

    /** Error stream. */
    protected OutputStream mErrorStream = null;

    /**
     * Default constructor.
     */
    public AbstractProcessOutputHandler() {
        mStandardStream = new ByteArrayOutputStream();
        mErrorStream = new ByteArrayOutputStream();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void linkToProcess(final ProcessLauncher pProcessLauncher) {
        if (pProcessLauncher != null) {
            pProcessLauncher.setErrorStream(mErrorStream);
            pProcessLauncher.setOutputStream(mStandardStream);
        }
    }

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public abstract IStatus computeStatus(final int pProcessResult);

    
    /**
     * {@inheritDoc}
     */
    @Override
    public OutputStream getStandardStream() {
        return mStandardStream;
    }

    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setStandardStream(final OutputStream pStandardStream) {
        mStandardStream = pStandardStream;
    }

    
    /**
     * {@inheritDoc}
     */
    @Override
    public OutputStream getErrorStream() {
        return mErrorStream;
    }

    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setErrorStream(final OutputStream pErrorStream) {
        mErrorStream = pErrorStream;
    }

}
