/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.importmodel.common.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.ui.command.AbstractCommandHandler;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.UMLFactory;
import org.polarsys.esf.core.importmodel.common.commands.AbstractImportModelCommand;

/**
 * Abstract handler class for import model.
 *
 * @author $Author: ymunoz $
 * @version $Revision: 168 $
 */
public abstract class AbstractImportModelHandler
    extends AbstractCommandHandler {

    /**
     * @return The GMF command instance
     */
    protected abstract ICommand getGMFCommand(TransactionalEditingDomain domain);

    /**
     * Get EMF command that wraps a GMF command.
     *
     * {@inheritDoc}
     */
    @Override
    protected Command getCommand(ExecutionEvent event) {
        ICommand vGMFCommand = getGMFCommand(getEditingDomain(event));
        return wrappedCommand(vGMFCommand);
    }

    @Override
    protected Command getCommand(IEvaluationContext context) {
    	 ICommand vGMFCommand = getGMFCommand(getEditingDomain(context));
    	 return wrappedCommand(vGMFCommand);
        
    }
    
    protected Command wrappedCommand(ICommand gmfCommand) {
    	 GMFtoEMFCommandWrapper vEMFCommand = new GMFtoEMFCommandWrapper(gmfCommand);

         if (gmfCommand instanceof AbstractImportModelCommand) {
             vEMFCommand.setDescription(((AbstractImportModelCommand) gmfCommand).getDescription());
         }
         return vEMFCommand;
    }

    /**
     * Creates a PackageImport in the current package, which refers to the
     * selected package.
     *
     * @param pPackage The selected package
     */
    protected void importPackage(final Package pPackage) {
        PackageImport vPackageImport = UMLFactory.eINSTANCE.createPackageImport();

        Package vImportedPackage = EMFHelper.reloadIntoContext(pPackage, getSelectedElement());

        ((Package) getSelectedElement()).getPackageImports().add(vPackageImport);
        vPackageImport.setImportedPackage(vImportedPackage);
    }
}
