/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.importmodel.common.dialogs;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.papyrus.uml.profile.ui.dialogs.ElementImportTreeSelectionDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.polarsys.esf.core.importmodel.common.ImportModelCommonActivator;

/**
 * An import model dialog, with the option to import a copy of the selected UML model.
 *
 * @author  $Author: ymunoz $
 * @version $Revision: 168 $
 */
public class UMLImportModelDialog
    extends ElementImportTreeSelectionDialog<Package> {

    /** Default label of Import All button. */
    private static final String IMPORT_ALL_LABEL =
        ImportModelCommonActivator.getMessages().getString("UMLImportModelDialog.button.importall"); //$NON-NLS-1$

    /** Default label of Deselect All button. */
    private static final String DESELECT_ALL_LABEL =
        ImportModelCommonActivator.getMessages().getString("UMLImportModelDialog.button.deselectall"); //$NON-NLS-1$

    /**
     * Default constructor.
     *
     * @param pParent The parent shell
     * @param pModel The UML model to import
     */
    public UMLImportModelDialog(Shell pParent, Package pModel) {
        super(pParent, ImportAction.IMPORT, Package.class, pModel);
    }

    /**
     * This constructor is used to import several models.
     *
     * @param pParent The parent shell
     * @param pModels The collection of the UML models to import
     */
    public UMLImportModelDialog(Shell pParent, Collection<? extends Package> pModels) {
        super(pParent, ImportAction.IMPORT, Package.class, pModels);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Composite createDialogArea(Composite pParent) {

        Composite vResult = (Composite) super.createDialogArea(pParent);

        Composite vButtonsComposite = new Composite(vResult, SWT.NONE);
        vButtonsComposite.setLayout(new RowLayout());

        Button vImportAllButton = new Button(vButtonsComposite, SWT.PUSH);
        vImportAllButton.setText(IMPORT_ALL_LABEL);
        vImportAllButton.addSelectionListener(new SelectionAdapter() {

            /**
             * {@inheritDoc}
             */
            @Override
            public void widgetSelected(final SelectionEvent pSEvent) {
                selectAll(ImportAction.IMPORT);
            }
        });

        Button vDeselectAllButton = new Button(vButtonsComposite, SWT.PUSH);
        vDeselectAllButton.setText(DESELECT_ALL_LABEL);
        vDeselectAllButton.addSelectionListener(new SelectionAdapter() {

            /**
             * {@inheritDoc}
             */
            @Override
            public void widgetSelected(final SelectionEvent pSEvent) {
                selectAll(ImportAction.NONE);
            }
        });

        return vResult;
    }

    /**
     * Retrieve the sub-packages of the package.
     *
     * @param pPackage The package
     * @return The collection of the sub-packages
     */
    @Override
    protected Collection<? extends Element> getChildren(final Package pPackage) {
        Collection<Package> vPackages = new ArrayList<Package>();

        for (Element vElement : pPackage.getPackagedElements()) {
            if (vElement instanceof Package) {
                vPackages.add((Package) vElement);
            }
        }

        return vPackages;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isResizable() {
        return true;
    }

}
