/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.rcp;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

/**
 * This class allow the manual build of the main menu and of the buttons bar.
 *
 * <p>
 * It's generally used to define the menu's main structure, by displaying the standard items used in Eclipse framework
 * ('Exit', 'About', 'Preferences', ...). Custom actions can be added can be added by coding them manually, but it's
 * advisable to use the extensions points 'org.eclipse.ui.menus' with the commands.
 * </p>
 *
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class ApplicationActionBarAdvisor
    extends ActionBarAdvisor {

    /** Label of the menu File. */
    private static final String MENU_FILE_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.file");

    /** Label of the menu Edit. */
    private static final String MENU_EDIT_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.edit");

    /** Label of the menu Window. */
    private static final String MENU_WINDOW_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.window");

    /** Label of the menu Window. */
    private static final String MENU_SHOWVIEW_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.window.showview");

    /** Label of the menu Window. */
    private static final String MENU_OPENPERSPECTIVE_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.window.openperspective");

    /** Label of the menu Help. */
    private static final String MENU_HELP_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.help");

    /** Label of the New menu. */
    private static final String MENU_FILE_NEW_LABEL = CoreRootActivator.getMessages().getString(
        "ApplicationActionBarAdvisor.file.new");

    /** Views short list contribution. */
    private ContributionItem mShowViewsContribution = null;

    /** Perspectives short list contribution. */
    private ContributionItem mOpenPerspectivesContribution = null;

    /** New wizard with drop down action. */
    private IWorkbenchAction mNewWizardDropDownAction = null;

    /** Help contents action. */
    private IWorkbenchAction mHelpContentsAction = null;

    /** Help search action. */
    private IWorkbenchAction mHelpSearchAction = null;

    /** Dynamic help action. */
    private IWorkbenchAction mDynamicHelpAction = null;

    /** Lock toolbar action. */
    private IWorkbenchAction mLockToolBarAction = null;

    /** Hide toolbar action. */
    private IWorkbenchAction mToggleCoolBarAction = null;

    /**
     * Default constructor.
     *
     * @param pConfigurer The action bar configurer
     */
    public ApplicationActionBarAdvisor(final IActionBarConfigurer pConfigurer) {
        super(pConfigurer);
    }

    /**
     * @see org.eclipse.ui.application.ActionBarAdvisor#makeActions(org.eclipse.ui.IWorkbenchWindow)
     *
     * Method used to build the actions to add to the menu and to the toolbar.
     * NB : The ActionFactory class is used to get the standard actions
     *
     * @param pWindow The workbench window
     */
    @Override
    protected final void makeActions(final IWorkbenchWindow pWindow) {

        /*
         * Create the contribution items to allow their use after
         */
        mShowViewsContribution = (ContributionItem) ContributionItemFactory.VIEWS_SHORTLIST.create(pWindow);
        mOpenPerspectivesContribution =
            (ContributionItem) ContributionItemFactory.PERSPECTIVES_SHORTLIST.create(pWindow);

        /*
         * Create each action and register it to allow their use in menu, toolbar, etc.
         */

        // The new wizard action can't be managed by a command,
        // that's why its manually added in the menu bar after
        // Moreover, the menu text is overloaded to make it more user friendly
        mNewWizardDropDownAction = ActionFactory.NEW_WIZARD_DROP_DOWN.create(pWindow);
        mNewWizardDropDownAction.setText(MENU_FILE_NEW_LABEL);
        register(mNewWizardDropDownAction);

        // The help content command isn't fully functional : it doesn't have its icon
        // that's why its created by an action
        mHelpContentsAction = ActionFactory.HELP_CONTENTS.create(pWindow);
        register(mHelpContentsAction);

        // The help search command isn't fully functional : it doesn't have its icon
        // that's why its created by an action
        mHelpSearchAction = ActionFactory.HELP_SEARCH.create(pWindow);
        register(mHelpSearchAction);

        // The dynamic help action is created and registered as an action
        mDynamicHelpAction = ActionFactory.DYNAMIC_HELP.create(pWindow);
        register(mDynamicHelpAction);

        // The lock tool bar is created and registered as an action to be used
        // in the cool bar context menu
        mLockToolBarAction = ActionFactory.LOCK_TOOL_BAR.create(pWindow);
        register(mLockToolBarAction);

        // The toggle cool bar is created and registered as an action to be used
        // in the cool bar context menu
        mToggleCoolBarAction = ActionFactory.TOGGLE_COOLBAR.create(pWindow);
        register(mToggleCoolBarAction);

        // Register the action to ensure that the associated command is working
        register(ActionFactory.RESET_PERSPECTIVE.create(pWindow));
        register(ActionFactory.SAVE_PERSPECTIVE.create(pWindow));
        register(ActionFactory.SHOW_EDITOR.create(pWindow));
        register(ActionFactory.REVERT.create(pWindow));
        register(ActionFactory.SAVE.create(pWindow));
        register(ActionFactory.SAVE_AS.create(pWindow));
        register(ActionFactory.SAVE_ALL.create(pWindow));
        register(ActionFactory.MOVE.create(pWindow));
        register(ActionFactory.REFRESH.create(pWindow));
        register(ActionFactory.UNDO.create(pWindow));
        register(ActionFactory.REDO.create(pWindow));
        register(ActionFactory.CUT.create(pWindow));
        register(ActionFactory.COPY.create(pWindow));
        register(ActionFactory.PASTE.create(pWindow));
        register(ActionFactory.DELETE.create(pWindow));

    }

    /**
     * @see org.eclipse.ui.application.ActionBarAdvisor#fillMenuBar(org.eclipse.jface.action.IMenuManager)
     *
     * Method used to build add the content to the menu bar given in parameter.
     *
     * @param pMenuBar The menu bar to fill
     */
    @Override
    protected final void fillMenuBar(final IMenuManager pMenuBar) {

        /*
         * File menu
         */
        final MenuManager vFileMenuMgr = new MenuManager(MENU_FILE_LABEL, IWorkbenchActionConstants.M_FILE);
        pMenuBar.add(vFileMenuMgr);

        // Add the new wizards menu in the file menu
        vFileMenuMgr.add(mNewWizardDropDownAction);

        /*
         * Edit menu
         */
        final MenuManager vEditMenu = new MenuManager(MENU_EDIT_LABEL, IWorkbenchActionConstants.M_EDIT);

        // Add the group marker used by open text file menu
        vEditMenu.add(new GroupMarker(IWorkbenchActionConstants.FIND_EXT));

        pMenuBar.add(vEditMenu);

        /*
         * Separator to prepare the menu insertion, for example from EMF, etc.
         */
        pMenuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

        /*
         * Window menu
         */
        final MenuManager vWindowMenuMgr = new MenuManager(MENU_WINDOW_LABEL, IWorkbenchActionConstants.M_WINDOW);
        pMenuBar.add(vWindowMenuMgr);

        final MenuManager vShowViewsMenu = new MenuManager(MENU_SHOWVIEW_LABEL);
        vShowViewsMenu.add(mShowViewsContribution);

        final MenuManager vOpenPerspectiveMenu = new MenuManager(MENU_OPENPERSPECTIVE_LABEL);
        vOpenPerspectiveMenu.add(mOpenPerspectivesContribution);

        vWindowMenuMgr.add(vOpenPerspectiveMenu);
        vWindowMenuMgr.add(vShowViewsMenu);

        /*
         * Help menu
         */
        createHelpMenu(pMenuBar);
    }

    /**
     * Create the help menu and its content.
     *
     * @param pMenuBar The parent menu manager which will contain the help menu
     */
    private void createHelpMenu(final IMenuManager pMenuBar) {
        // Create the help menu and add it to the parent
        final MenuManager vHelpMenuMgr = new MenuManager(MENU_HELP_LABEL, IWorkbenchActionConstants.M_HELP);
        pMenuBar.add(vHelpMenuMgr);

        // First add the main help actions
        vHelpMenuMgr.add(mHelpContentsAction);
        vHelpMenuMgr.add(mHelpSearchAction);
        vHelpMenuMgr.add(mDynamicHelpAction);

        // Then add the standard group marker and separator used by external contributions
        vHelpMenuMgr.add(new Separator(IWorkbenchActionConstants.HELP_START));
        vHelpMenuMgr.add(new GroupMarker("group.main.ext")); //$NON-NLS-1$
        vHelpMenuMgr.add(new GroupMarker("group.tutorials")); //$NON-NLS-1$
        vHelpMenuMgr.add(new GroupMarker("group.tools")); //$NON-NLS-1$
        vHelpMenuMgr.add(new GroupMarker("group.updates")); //$NON-NLS-1$
        vHelpMenuMgr.add(new GroupMarker(IWorkbenchActionConstants.HELP_END));
        vHelpMenuMgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

        // Finally, add the About group at the bottom
        vHelpMenuMgr.add(new Separator("group.about")); //$NON-NLS-1$
    }

    /**
     * @see org.eclipse.ui.application.ActionBarAdvisor#fillCoolBar(org.eclipse.jface.action.ICoolBarManager)
     * @param pCoolBar The coolbar to fill
     */
    @Override
    protected final void fillCoolBar(final ICoolBarManager pCoolBar) {
        /*
         * File part in the toolbar
         */
        final IToolBarManager vFileToolBar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
        pCoolBar.add(new ToolBarContributionItem(vFileToolBar, IWorkbenchActionConstants.TOOLBAR_FILE));

        vFileToolBar.add(mNewWizardDropDownAction);

        /*
         * Cool bar context menu (right click)
         */
        final MenuManager vContextMenuMgr = new MenuManager();
        pCoolBar.setContextMenuManager(vContextMenuMgr);

        vContextMenuMgr.add(mLockToolBarAction);
        vContextMenuMgr.add(mToggleCoolBarAction);
    }
}
