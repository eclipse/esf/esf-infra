/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.ui;

import java.util.ResourceBundle;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.dialogs.WorkbenchWizardElement;
import org.eclipse.ui.internal.wizards.AbstractExtensionWizardRegistry;
import org.eclipse.ui.internal.wizards.ExportWizardRegistry;
import org.eclipse.ui.internal.wizards.ImportWizardRegistry;
import org.eclipse.ui.internal.wizards.NewWizardRegistry;
import org.polarsys.esf.core.common.messages.Messages;

/**
 * The activator class controls the plug-in life cycle. It also provide an access to utility methods.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 99 $
 */
public class CoreUIActivator
    extends EMFPlugin {

    /** The plug-in ID. */
    public static final String PLUGIN_ID = "org.polarsys.esf.core.ui"; //$NON-NLS-1$

    /** The singleton instance of this class. */
    private static final CoreUIActivator INSTANCE = new CoreUIActivator();

    /** The shared instance of this plugin. */
    private static Implementation sPlugin = null;

    /** Messages class used to find localised string. */
    private static Messages sMessages = new Messages(ResourceBundle.getBundle(Messages.BUNDLE_NAME));

    /**
     * The default constructor.
     */
    public CoreUIActivator() {
        super(new ResourceLocator[] {});
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     * 
     * @return The singleton instance
     */
    @Override
    public final ResourceLocator getPluginResourceLocator() {
        return sPlugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin.
     * 
     * @return The singleton instance
     */
    public static Implementation getPlugin() {
        return sPlugin;
    }

    /**
     * @return The messages class used to return localised string
     */
    public static Messages getMessages() {
        return sMessages;
    }

    /**
     * Create an Error status with the data given in parameter and log it.
     * 
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logError(final String pMessage, final Exception pException) {
        // Create the Error status
        IStatus vStatus = new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create a Warning status with the data given in parameter and log it.
     * 
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logWarning(final String pMessage, final Exception pException) {
        // Create the Warning status
        IStatus vStatus = new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * Create an Info status with the data given in parameter and log it.
     * 
     * @param pMessage The message to log
     * @param pException The exception to log
     */
    public static void logInfo(final String pMessage, final Exception pException) {
        // Create the Info status
        IStatus vStatus = new Status(IStatus.INFO, PLUGIN_ID, IStatus.INFO, pMessage, pException);

        // Log it
        INSTANCE.log(vStatus);
    }

    /**
     * The actual implementation of the Eclipse <b>Plugin</b>.
     */
    public static class Implementation
        extends EclipseUIPlugin {

        /** The key of the file used in the creation wizard for ESF project. */
        public static final String ICON_WIZARD_SA_PROJECT = "image.new.project.sa"; //$NON-NLS-1$

        /** Icons folder. */
        private static final String ICONS_FOLDER = "src/main/resources/icons/"; //$NON-NLS-1$

        /**
         * Id list of new wizards to hide.
         */
        private static final String[] NEW_WIZARDS_TOHIDE_IDS_ARRAY =
        {"org.eclipse.emf.ecore.presentation.EcoreModelWizardID", //$NON-NLS-1$
            "org.eclipse.ui.wizards.new.project", //$NON-NLS-1$
            "org.eclipse.ui.wizards.new.folder", //$NON-NLS-1$
            "org.eclipse.ui.wizards.new.file", //$NON-NLS-1$
            "org.eclipse.ui.editors.wizards.UntitledTextFileWizard"}; //$NON-NLS-1$

        /**
         * Id list of import wizards to hide.
         */
        private static final String[] IMPORT_WIZARDS_TOHIDE_IDS_ARRAY =
        {"org.eclipse.equinox.p2.replication.import", //$NON-NLS-1$
            "org.eclipse.equinox.p2.replication.importfrominstallation"}; //$NON-NLS-1$

        /**
         * Id list of export wizards to hide.
         */
        private static final String[] EXPORT_WIZARDS_TOHIDE_IDS_ARRAY =
        {"org.eclipse.equinox.p2.replication.export"}; //$NON-NLS-1$

        /**
         * Creates an instance.
         */
        public Implementation() {
            super();

            // Remember the static instance
            sPlugin = this;

            // Remove wizard for model file, as user don't want it, but we don't want to remove it from code
            hideUselessWizard();

        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void initializeImageRegistry(final ImageRegistry pRegistry) {
            super.initializeImageRegistry(pRegistry);

            pRegistry.put(
                ICON_WIZARD_SA_PROJECT,
                imageDescriptorFromPlugin(getSymbolicName(), 
                ICONS_FOLDER + "wizards/wizard_project.gif")); //$NON-NLS-1$
        }

        /**
         * Method used to hide useless wizards we don't want or can't remove from code,
         * but which are not needed by the users.
         */
        @SuppressWarnings("restriction")
        private void hideUselessWizard() {

            // Get wizard registry for new part
            NewWizardRegistry vNewWizardRegistry = (NewWizardRegistry) PlatformUI
                .getWorkbench()
                .getNewWizardRegistry();

            removeWizardFromRegistry(NEW_WIZARDS_TOHIDE_IDS_ARRAY, vNewWizardRegistry);

            // Get wizard registry for import part
            ImportWizardRegistry vImportWizardRegistry =
                (ImportWizardRegistry) PlatformUI.getWorkbench().getImportWizardRegistry();

            removeWizardFromRegistry(IMPORT_WIZARDS_TOHIDE_IDS_ARRAY, vImportWizardRegistry);

            // Get wizard registry for export part
            ExportWizardRegistry vExportWizardRegistry =
                (ExportWizardRegistry) PlatformUI.getWorkbench().getExportWizardRegistry();

            removeWizardFromRegistry(EXPORT_WIZARDS_TOHIDE_IDS_ARRAY, vExportWizardRegistry);
        }

        /**
         * Remove a list of wizards from a specific wizard registry.
         * 
         * @param pWizardIdsArray Array of id used to identify wizards to remove
         * @param pTargetRegistry Registry from which wizards will be removed
         */
        @SuppressWarnings("restriction")
        private void removeWizardFromRegistry(final String[] pWizardIdsArray,
            final AbstractExtensionWizardRegistry pTargetRegistry) {
            
            for (String vWizardId : pWizardIdsArray) {
                // Find wizard and cast it as wizard element
                WorkbenchWizardElement vWizardElement = (WorkbenchWizardElement) pTargetRegistry.findWizard(vWizardId);

                if (vWizardElement != null) {
                    // Remove it from registry
                    pTargetRegistry.removeExtension(
                        vWizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] {vWizardElement});
                }

            }
        }

    }

}
