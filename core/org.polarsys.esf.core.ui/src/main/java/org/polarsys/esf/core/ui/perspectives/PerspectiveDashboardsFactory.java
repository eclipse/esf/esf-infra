/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.ui.perspectives;

import org.eclipse.papyrus.views.modelexplorer.ModelExplorerPageBookView;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Perspective Dashboards definition.
 *
 * @author $Author: jdumont $
 * @version $Revision: 90 $
 */
public class PerspectiveDashboardsFactory
    implements IPerspectiveFactory {

    /** ID of dashboard view. */
    private static final String ID_DASHBOARD_VIEW = "org.polarsys.esf.analysis.core.ui.dashboard"; //$NON-NLS-1$

    /** ID of failure mode view. */
    private static final String ID_FAILURE_MODE_VIEW = "org.polarsys.esf.analysis.core.ui.failuremode"; //$NON-NLS-1$

    /** ID of ESF log view. */
    private static final String ID_SA_LOG_VIEW = "org.polarsys.esf.view.log"; //$NON-NLS-1$

    /** ID of the folder placed on the bottom. */
    private static final String ID_FOLDER_BOTTOM = "bottomFolderId"; //$NON-NLS-1$

    /** ID of the folder placed on the top left. */
    private static final String ID_FOLDER_TOPLEFT = "topLeftFolderId"; //$NON-NLS-1$

    /** ID of the folder placed on the bottom left. */
    private static final String ID_FOLDER_BOTTOMLEFT = "bottomLeftFolderId"; //$NON-NLS-1$

    /** ID of the folder placed on the top right. */
    private static final String ID_FOLDER_TOPRIGHT = "topRightFolderId"; //$NON-NLS-1$

    /** ID of the folder placed on the right. */
    private static final String ID_FOLDER_RIGHT = "RightFolderId"; //$NON-NLS-1$

    /**
     * Default constructor.
     */
    public PerspectiveDashboardsFactory() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void createInitialLayout(final IPageLayout pLayout) {
        // Get the editor area
        String vEditorArea = pLayout.getEditorArea();

        // On the top left : Project Explorer view
        IFolderLayout vTopLeftPlace = pLayout.createFolder(ID_FOLDER_TOPLEFT, IPageLayout.LEFT, 0.25f, vEditorArea);
        vTopLeftPlace.addView(IPageLayout.ID_PROJECT_EXPLORER);

        // At the bottom : Property sheet, Log view
        IFolderLayout vBottomFolder =
            pLayout.createFolder(ID_FOLDER_BOTTOM, IPageLayout.BOTTOM, 0.66f, vEditorArea);
        vBottomFolder.addView(IPageLayout.ID_PROP_SHEET);
        vBottomFolder.addView(ID_SA_LOG_VIEW);

        // At top right: Blocks view
        IFolderLayout vTopRightFolder =
            pLayout.createFolder(ID_FOLDER_TOPRIGHT, IPageLayout.RIGHT, 0.55f, vEditorArea);
        vTopRightFolder.addView(ID_DASHBOARD_VIEW);

        // At right: Failure modes view
        IFolderLayout vRightFolder =
            pLayout.createFolder(ID_FOLDER_RIGHT, IPageLayout.BOTTOM, 0.55f, ID_FOLDER_TOPRIGHT);
        vRightFolder.addView(ID_FAILURE_MODE_VIEW);

        // At the bottom left, Outline and Model explorer
        IFolderLayout vBottomLeftFolder =
            pLayout.createFolder(ID_FOLDER_BOTTOMLEFT, IPageLayout.BOTTOM, 0.5f, ID_FOLDER_TOPLEFT);
        vBottomLeftFolder.addView(IPageLayout.ID_OUTLINE);
        vBottomLeftFolder.addView(ModelExplorerPageBookView.VIEW_ID);

    }
}
