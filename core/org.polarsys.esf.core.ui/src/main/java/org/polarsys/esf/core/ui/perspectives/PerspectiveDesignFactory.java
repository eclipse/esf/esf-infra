/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.ui.perspectives;

import org.eclipse.papyrus.views.modelexplorer.ModelExplorerPageBookView;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Perspective Design definition.
 *
 * It contains some views from other plugins, to allow for example the use of the graphical modellers, and some views
 * used to manage projects, etc.
 *
 * @author $Author: jdumont $
 * @version $Revision: 90 $
 */
public class PerspectiveDesignFactory
    implements IPerspectiveFactory {

    /** ID of ESF log view. */
    private static final String ID_SA_LOG_VIEW = "org.polarsys.esf.view.log"; //$NON-NLS-1$

    /** ID of the folder placed on the bottom. */
    private static final String ID_FOLDER_BOTTOM = "bottomFolderId"; //$NON-NLS-1$

    /** ID of the folder placed on the top left. */
    private static final String ID_FOLDER_TOPLEFT = "topLeftFolderId"; //$NON-NLS-1$

    /** ID of the folder placed on the bottom left. */
    private static final String ID_FOLDER_BOTTOMLEFT = "bottomLeftFoldId"; //$NON-NLS-1$


    /**
     * Default constructor.
     */
    public PerspectiveDesignFactory() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void createInitialLayout(final IPageLayout pLayout) {

        // Get the editor area
        String vEditorArea = pLayout.getEditorArea();

        // On the top left : Project Explorer view
        IFolderLayout vTopLeftPlace = pLayout.createFolder(ID_FOLDER_TOPLEFT, IPageLayout.LEFT, 0.25f, vEditorArea);
        vTopLeftPlace.addView(IPageLayout.ID_PROJECT_EXPLORER);

        // At the bottom : Property sheet and Log view
        IFolderLayout vBottomFolder = pLayout.createFolder(ID_FOLDER_BOTTOM, IPageLayout.BOTTOM, 0.66f, vEditorArea);
        vBottomFolder.addView(IPageLayout.ID_PROP_SHEET);
        vBottomFolder.addView(ID_SA_LOG_VIEW);

        // At the bottom left : Outline and Model explorer
        IFolderLayout vBottomLeftFolder =
            pLayout.createFolder(ID_FOLDER_BOTTOMLEFT, IPageLayout.BOTTOM, 0.5f, ID_FOLDER_TOPLEFT);
        vBottomLeftFolder.addView(IPageLayout.ID_OUTLINE);
        vBottomLeftFolder.addView(ModelExplorerPageBookView.VIEW_ID);
    }
}
