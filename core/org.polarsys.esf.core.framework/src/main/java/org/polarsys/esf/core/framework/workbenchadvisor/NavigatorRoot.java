/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.framework.workbenchadvisor;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.model.IWorkbenchAdapter;

/**
 * Wrapper class for workbench root from <code>ResourcesPlugin.getWorkspace().getRoot()</code>.
 *
 * It ensures that the workspace root conforms to IPersistableElement according to
 * <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=145233">bug 145233</a>.
 *
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public class NavigatorRoot
    implements IAdaptable, IPersistableElement, IElementFactory {

    /**
     * Default constructor.
     */
    public NavigatorRoot() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Object getAdapter(@SuppressWarnings("rawtypes") final Class pClass) {
        Object vAdapter = null;

        // Get the adapter according to the expected class given in parameter
        if (IPersistableElement.class.equals(pClass)) {
            // ... For a persitable element, use directly this instance
            vAdapter = this;

        } else if (IWorkbenchAdapter.class.equals(pClass)) {
            // ... For a workbench adapter, ask the workspace root to find the right adapter
            vAdapter = ResourcesPlugin.getWorkspace().getRoot().getAdapter(pClass);

        } else if (IResource.class.isAssignableFrom(pClass)) {
            // ... For any kind of resource (project, file, etc.), ask the workspace root to find the right adapter
            vAdapter = ResourcesPlugin.getWorkspace().getRoot().getAdapter(pClass);
        }

        return vAdapter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String getFactoryId() {
        return this.getClass().getCanonicalName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void saveState(final IMemento pMemento) {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final IAdaptable createElement(final IMemento pMemento) {
        // Return by default the workspace root
        return ResourcesPlugin.getWorkspace().getRoot();
    }
}
