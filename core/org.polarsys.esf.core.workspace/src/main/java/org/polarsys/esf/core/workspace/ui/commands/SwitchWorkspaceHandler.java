/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.workspace.ui.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.polarsys.esf.core.workspace.WorkspaceActivator;
import org.polarsys.esf.core.workspace.ui.dialogs.SelectWorkspaceDialog;

/**
 * Handler used for our custom command allowing the user to switch of workspace.
 * 
 * @author $Author: jdumont $
 * @version $Revision: 83 $
 */
public class SwitchWorkspaceHandler
    extends AbstractHandler {

    /**
     * Default constructor.
     */
    public SwitchWorkspaceHandler() {
        // Call the parent constructor
        super();
    }

    /**
     * {@inheritDoc}
     * 
     * Open a dialog to let the user select its workspace, and restart if he validates its choice.
     */
    @Override
    public Object execute(final ExecutionEvent pEvent) throws ExecutionException {

        // Create the dialog to select the workspace
        SelectWorkspaceDialog vSwitchWorkspaceDialog = new SelectWorkspaceDialog(true);

        // Open the dialog and get its returned value
        if (vSwitchWorkspaceDialog.open() == Dialog.OK) {
            // The user as decided to change its workspace,
            // the application must be restarted
            MessageDialog.openInformation(
                Display.getDefault().getActiveShell(),
                WorkspaceActivator.getMessages().
                    getString("SwitchWorkspaceHandler.restart.dialog.title"), //$NON-NLS-1$
                WorkspaceActivator.getMessages().
                    getString("SwitchWorkspaceHandler.restart.dialog.message")); //$NON-NLS-1$

            // Restart the application
            PlatformUI.getWorkbench().restart();
        }

        return null;
    }

}
