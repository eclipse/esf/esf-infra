/*******************************************************************************
 * Copyright (c) 2016 ALL4TEC & CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     ALL4TEC & CEA LIST - initial API and implementation
 ******************************************************************************/
package org.polarsys.esf.core.workspace.utils;

import java.util.Set;


/**
 * Interface for all the strategies corresponding to the behaviour to save 
 * and reload the workspace preferences. 
 * 
 * @author  $Author: jdumont $
 * @version $Revision: 83 $
 */
public interface IWorkspacePreferencesStrategy {
    
    /**
     * Return the set of workspaces available, stored in the preferences. 
     * This set is initialised with the workspace previously used and known,
     * and the default workspace value.
     * 
     * @return The set of workspaces known
     */
    Set<String> getAvailableWorkspacesSet();
    
    /**
     * Return the value of the preference used to specify if the user
     * wants to remember of his workspace choice for the next time.
     * 
     * @return The value of the preference 
     */
    boolean isRememberWorkspace();
    
    /**
     * Return the last workspace path saved in the preferences.
     * 
     * @return The last workspace path saved, or null if none
     */
    String getLastWorkspacePath();
    
    /**
     * Save the preferences relative to the workspace properties :
     * - The current workspace path.
     * - A flag specifying if the user decided to remember of his choice and thus to don't be prompt next time.
     * - The set of all the available workspaces.
     * 
     * @param pWorkspacePath The path of the current workspace used  
     * @param pRememberWorkspace <code>true</code> if the current workspace choice must be remembered 
     *                           for the next time, to open it without asking to the user
     * @param pAvailableWorkspacesSet The set of all the available workspaces
     */
    void saveWorkspacePreferences(
        final String pWorkspacePath, 
        final boolean pRememberWorkspace,
        final Set<String> pAvailableWorkspacesSet);
    
    
    /**
     * Clear the preferences relative to the workspace properties.
     */
    void clearWorkspacePreferences();
}
